import pygame               #This is the main file that contains the main functions, classes and system loops
import random as r
import MapBuilder
import sys
from PIL import Image, ImageDraw
import numpy as np
from threading import Thread
from shapely.geometry import Point, Polygon
from math import sin, cos, pi
import HousePresets
from functools import lru_cache
import time

Clock = pygame.time.Clock()                                     #Global Variables
pygame.init()
Screen = pygame.display.set_mode((1150, 850))
Screen.fill((0,0,0))
Done = False
LoadingProgress = (0,0)

Seed = (r.randint(-80000,80000), r.randint(-80000,80000))
Snow = 200

layer = 0
currentTown = 0
Towns = []
currentHouse = 0
RealmName = ''
HelpMenuActive = False

buildings = [   #Images of each type of building
    pygame.image.load("Images/Buildings/Ahouse.png").convert_alpha(),
    pygame.image.load("Images/Buildings/Squarehouse.png").convert_alpha(),
    pygame.image.load("Images/Buildings/Tower.png").convert_alpha(),
    pygame.image.load("Images/Buildings/Tavern.png").convert_alpha(),
    pygame.image.load("Images/Buildings/Blacksmith.png").convert_alpha(),
    pygame.image.load("Images/Buildings/Church.png").convert_alpha()
]

GUI = [         #Images for GUI elements
    pygame.image.load("Images/Other/Frame.png").convert_alpha(),
    pygame.image.load("Images/Other/FrameMain.png").convert_alpha(),
    pygame.image.load("Images/Other/NamePlate.png").convert_alpha(),
    pygame.image.load("Images/Other/NamePlate2.png").convert_alpha(),
    pygame.image.load("Images/Other/Info.png").convert_alpha(),
    pygame.image.load("Images/Other/Button.png").convert_alpha(),
    pygame.image.load("Images/Other/InfoPressed.png").convert_alpha(),
    pygame.image.load("Images/Other/ButtonPressed.png").convert_alpha(),
    pygame.image.load("Images/Other/LoadingScreen.png").convert_alpha(),
    pygame.image.load("Images/Other/LoadingBar.png").convert_alpha()
]

class Button:   #Button class that can tell if it has been clicked. Also stores the path to its function using lambda functions
    def __init__(self,polygon,function,isClickable):
        self.polygon = polygon
        self.function = function
        self.isClickable = isClickable

    def DetectClick(self, mx, my):
        if Point(mx,my).intersects(Polygon(self.polygon)):
            return True

class Town:     #Town class stores data about the town as well as the button used to navigate to it and lists of data relating to the houses in the town. Has a function to create its own image (draw()) and to make its out description (info())
    def __init__(self, pos, index, name):
        self.pos = pos
        self.index = index
        self.image = 0
        self.image2 = 0
        self.button = Button( [ (self.pos[0]*7/8 + 400,self.pos[1]*7/8 + 100), (self.pos[0]*7/8 + 420,self.pos[1]*7/8 + 100), (self.pos[0]*7/8 + 420,self.pos[1]*7/8 + 120), (self.pos[0]*7/8 + 400,self.pos[1]*7/8 + 120) ] , lambda: ChangeTown(self.index), True)
        self.houses = []
        self.houseObjects = []
        self.name = name
        self.description = ''

    def Draw(self):     #Function used to create an image for the town
        import TownBuilder
        TownBuilder.CreateTown(self.index)
        MapBuilder.GenerateTownRegion(32*Seed[0]+32*self.pos[0], 32*Seed[1]+32*self.pos[1], (400,400), Snow, 'CompletedTown'+str(self.index), 32*self.pos[1], pygame.image.load("ImageDump/town"+str(self.index)+".png").convert_alpha())
        self.houses = TownBuilder.houses
        sys.modules.pop('TownBuilder')
        self.Info()

    def Info(self):     #Function used to generate a description for the town based on how many houses are in it
        self.description = 'A small island town|with a population of|' + str(len(self.houses)*4+r.randint(0,1)) + "."

class House:        #House class stores data about the house as well the Interactable objects inside it. It also contains fucntions to populate it.
    def __init__(self, bounds, TownNumber, HouseNumber, HouseType, HouseImageNumber, dims, normal):
        self.bounds = bounds
        self.TownNumber = TownNumber
        self.HouseNumber = HouseNumber
        self.HouseType = HouseType
        self.HouseImageNumber = HouseImageNumber
        self.icon = buildings[self.HouseImageNumber]
        self.image = 0
        self.button = Button([ self.bounds[0],self.bounds[1],self.bounds[2],self.bounds[3] ], lambda: ChangeHouse(self.TownNumber, self.HouseNumber), True)
        self.dims = dims
        self.normal = normal
        self.interactables = []
        self.name = ''
        self.description = ''
        for x in range(10+self.dims[0]):
            self.interactables.append([])
            for y in range(10+self.dims[1]):
                self.interactables[x].append(0)

        self.Info()

    def Draw(self):     #creates a floorplan image
        import HouseBuilder
        HouseBuilder.GenerateAll(self.HouseType, self.HouseImageNumber, self.TownNumber, self.HouseNumber)
        self.image = pygame.image.load('ImageDump/house'+str(self.TownNumber)+'_'+str(self.HouseNumber)+'.png').convert_alpha()
        sys.modules.pop('HouseBuilder')

    def Info(self):     #creates a description depending on the type of house
        if self.HouseImageNumber == 0:
            self.name = 'House'
            self.description = "A small village|house with an|A-Frame roof."
        elif self.HouseImageNumber == 1:
            self.name = 'Hut'
            self.description = "A small peasant|house with a flat|roof."
        elif self.HouseImageNumber == 2:
            self.name = 'Tower'
            self.description = "A stone tower that|houses the town's|weapons, tools and|armour."
        elif self.HouseImageNumber == 3:
            self.name = 'Tavern'
            self.description = "A hub for locals and|travellers alike,|the tavern is the|heart of the town."
        elif self.HouseImageNumber == 4:
            self.name = 'Blacksmith'
            self.description = "A larger house|outfitted with a|workshop where the|blacksmith makes|tools, weapons and|armour for the town."
        else:
            self.name = 'Church'
            self.description = "A holy building were|people congregate in|worship."

    def CreateInterior(self):   #Loads presets for the arrangement of interactable objects depending on the house
        if self.HouseType == 0:
            if self.HouseImageNumber == 2:
                HousePresets.TowerPreset(self.interactables)
            else:
                HousePresets.HousePreset(self.interactables)
        if self.HouseType == 1:
            if self.HouseImageNumber == 3:
                HousePresets.TavernPreset(self.interactables)
            if self.HouseImageNumber == 4:
                HousePresets.BlacksmithPreset(self.interactables)
        if self.HouseType == 2:
            HousePresets.ChurchPreset(self.interactables)


def ChangeTown(TownNumber):                 #Changes layers from 0 to 1 and generates houses for the selected town
    global currentTown
    global layer
    global Towns
    currentTown = TownNumber
    layer = 1
    if Towns[TownNumber].houseObjects == []:
        for h in Towns[TownNumber].houses:
            Towns[TownNumber].houseObjects.append( House( ([ (21/32*h[0][0] +488, 21/32*h[0][1] + 188), (21/32*h[1][0] +488, 21/32*h[1][1] + 188), (21/32*h[2][0] +488, 21/32*h[2][1] +188), (21/32*h[3][0] +488, 21/32*h[3][1] +188) ]), TownNumber, Towns[TownNumber].houses.index(h), h[5], h[7], h[6], h[4]*pi/180 ) )
    for i in Towns:
        i.button.isClickable = False

def ChangeHouse(TownNumber, HouseNumber):   #Changes layers from 1 to 2 and generates the interior of the house
    global currentHouse
    global layer
    currentHouse = HouseNumber
    layer = 2
    if Towns[TownNumber].houseObjects[HouseNumber].image == 0:
        Towns[TownNumber].houseObjects[HouseNumber].Draw()
        Towns[TownNumber].houseObjects[HouseNumber].CreateInterior()
    for i in Towns[TownNumber].houseObjects:
        i.button.isClickable = False

def ToggleHelpMenu():           #Toggles the help menu when the (i) button is clicked
    global HelpMenuActive
    if HelpMenuActive == True:
        HelpMenuActive = False
    else:
        HelpMenuActive = True

def HelpMenuLayer():            #Draws the help menu layer, providing different info depending on the location (layer) that the user is on
    global HelpMenuActive
    global layer
    if HelpMenuActive == True:
        pygame.draw.rect(Screen, (59,32,15), (740,75,300,300))
        pygame.draw.rect(Screen, (255,253,231), (740,75,300,300), 3)
        if layer == 0:
            text = "HOVER mouse over|towns to get info|and CLICK to travel|to them. Press [ESC]|to return to|previous location."
            TextWrap(text, (760, 95), 25, (255, 253, 231), 'Mairon-KYre.ttf', 25)
            TextWrap("CLICK 'Quit' to|close the program.", (760, 295), 25, (255, 253, 231), 'Mairon-KYre.ttf', 25)
            num = ''
            for i in Towns:
                if i.pos[0] <= 300:
                    num = i.index
                    break
            if num == '':
                num = -1
            pygame.draw.line(Screen, (255,0,0), (735, 150), (Towns[num].button.polygon[1][0], Towns[num].button.polygon[1][1] + 12), 3)
            pygame.draw.line(Screen, (255,0,0), (735, 320), ((155, 775)), 3)
        elif layer == 1:
            text = "HOVER mouse over|buildings to get info|and CLICK to travel|to them. Press [ESC]|to return to|previous location."
            TextWrap(text, (760, 95), 25, (255, 253, 231), 'Mairon-KYre.ttf', 25)
            TextWrap("CLICK 'Quit' to|close the program.", (760, 295), 25, (255, 253, 231), 'Mairon-KYre.ttf', 25)
            num = ''
            for i in Towns[currentTown].houseObjects:
                if i.bounds[0][0] <= 300:
                    num = Towns[currentTown].houseObjects.index(i)
                    break
            if num == '':
                num = -1
            pygame.draw.line(Screen, (255,0,0), (735, 150), (Towns[currentTown].houseObjects[-1].button.polygon[1][0], Towns[currentTown].houseObjects[-1].button.polygon[1][1] + 12), 3)
            pygame.draw.line(Screen, (255,0,0), (735, 320), ((155, 775)), 3)
        else:
            text = "HOVER mouse over|objects to get info|Press [ESC] to|return to previous|location."
            TextWrap(text, (760, 95), 25, (255, 253, 231), 'Mairon-KYre.ttf', 25)
            TextWrap("CLICK 'Quit' to|close the program.", (760, 295), 25, (255, 253, 231), 'Mairon-KYre.ttf', 25)
            for x in Towns[currentTown].houseObjects[currentHouse].interactables[::-1]:
                for y in x:
                    if y != 0:
                        pos = (410+34*(6-Towns[currentTown].houseObjects[currentHouse].dims[0]/4 + Towns[currentTown].houseObjects[currentHouse].interactables.index(x)), 110+34*(5-Towns[currentTown].houseObjects[currentHouse].dims[1]/4 + x.index(y)))
                        break
            pygame.draw.line(Screen, (255,0,0), (735, 150), (pos), 3)
            pygame.draw.line(Screen, (255,0,0), (735, 320), ((155, 775)), 3)

def Text(text, position, size, colour, textfont):                   #Draws text to the screen that is centred on the position given
    font = pygame.font.Font(textfont, size)
    text = font.render(text, True, colour)
    textRect = text.get_rect()
    textRect.center = position
    Screen.blit(text, textRect)

def TextWrap(text, position, size, colour, textfont, spacing):      #Draws text on the screen that wraps where the '|' is placed convetionally (top right corner is the position given)
    Lines = text.split('|')
    for line in Lines:
        font = pygame.font.Font(textfont, size)
        text = font.render(line, True, colour)
        textRect = text.get_rect()
        textRect.topleft = (position[0], position[1] + spacing*Lines.index(line))
        Screen.blit(text, textRect)

def TownLocations(Locations):           #Linear search of a 2D array (image) to find ideal tiles to place a town on
    image = Image.open('ImageDump/Map.png').convert('RGB')
    imageArray  = np.array(image)
    xPass = 0
    yPass = 0
    for x in range(32):
        if xPass == 0:
            for y in range(32):
                if yPass == 0:
                    CorrectColours = []
                    for i in range(x*25, (x+1)*25):
                        for j in range(y*25, (y+1)*25):
                            if int(imageArray[i][j][1]) >= 70 and list(imageArray[i][j]) != [160,160,220]:
                                CorrectColours.append((i,j))
                    if len(CorrectColours) >= 594:  #95% (594/625) of the pixels have to be correct for the tile to be suitable
                        Locations.append((25*y,25*x))
                        xPass = 1
                        yPass = 1

                else:
                    yPass = 0
        else:
            xPass = 0
    image.save("ImageDump/Map.png")

@lru_cache(maxsize=65000)   #Allowing for all iterations of the perlin noise loop to be cached for speed purposes
def GenerateAll():          #A function that executes all the other functions for the main generation sequence that occurs at the start
    global Done
    global LoadingProgress
    global RealmName

    RealmNames = []
    for line in open('Text/RealmNames.txt', 'r'):
        RealmNames.append(line)

    RealmName = RealmNames[r.randint(0,len(RealmNames) - 1)][:-1]

    TownNames = []
    for line in open('Text/TownNames.txt', 'r'):
        TownNames.append(line)

    MapBuilder.GenerateRegion(Seed[0], Seed[1], (800,800), Snow, 'Map')
    LoadingProgress = (1,1)

    Locations = []
    TownLocations(Locations)


    for i in Locations:
        LoadingProgress = (LoadingProgress[0], LoadingProgress[1]+1)
        Name = r.randint(0,len(TownNames) - 1)
        Towns.append(Town(i, Locations.index(i), TownNames[Name][:-1]))
        TownNames.remove(TownNames[Name])

    for i in Towns:     #Each town is executed in its own thread for the sake of speed
        Thread(target = i.Draw(), args = ()).start()
        LoadingProgress = (LoadingProgress[0]+1, LoadingProgress[1])
    Done = True
    sys.exit()

T = Thread(target = GenerateAll, args = ())
T.start()

(1050,22)
InfoButton = Button( ( (1050,22), (1110,22), (1110,82), (1050,82) ), lambda: ToggleHelpMenu(), True)        #The only 2 buttons in the game, everything else is part of the interactive map
QuitButton = Button( ( (50,750), (150,750), (150,800), (50,800) ), lambda: pygame.quit(), True)

frame = 0
loadingText = 'Loading'
while Done == False:    #Loop that occurs during the loading process
    Clock.tick(60)
    frame += 1
    Screen.fill((59,32,15))
    Screen.blit(GUI[8], (0,0))

    if frame % 20 == 0:
        loadingText = loadingText + ' .'
        if loadingText == 'Loading . . . .':
            loadingText = 'Loading'

    TextWrap(loadingText, (50,35), 50, (255, 253, 231), 'Mairon-KYre.ttf', 30)

    pygame.draw.rect(Screen, (59,32,15), (370,370,410,60))
    Screen.blit(GUI[9], (355,363))
    if LoadingProgress[0] == 1:
        pygame.draw.rect(Screen, (255, 253, 231), (375,375,100,50))
    if LoadingProgress[0] > 1 and LoadingProgress[1] >= 1:
        pygame.draw.rect(Screen, (255, 253, 231), (375,375,100+300*(LoadingProgress[0]/LoadingProgress[1]),50))

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
    pygame.display.update()

for i in Towns:
    i.image = pygame.transform.scale(pygame.image.load("ImageDump/town"+str(i.index)+".png").convert_alpha(), (20,20))
    i.image2 = pygame.transform.scale(pygame.image.load("ImageDump/CompletedTown"+str(i.index)+".png").convert_alpha(), (700,700))

while Done == True:     #Main game loop
    mx, my = pygame.mouse.get_pos()
    Screen.fill((59,32,15))

    if layer == 0:  #Layer 0 (region map)
        Screen.blit(pygame.transform.scale(pygame.image.load("ImageDump/Map.png").convert_alpha(), (700,700)), (400,100))
        for i in Towns:
            Screen.blit(i.image, (i.pos[0]*7/8 + 400, i.pos[1]*7/8 + 100))
            if i.button.DetectClick(mx, my) == True:
                pygame.draw.circle(Screen, (255,0,0), (7/8*i.pos[0] + 410, 7/8*i.pos[1] + 110), 15, 2)
                Screen.blit(GUI[2], (50,100))
                Text(i.name, (200, 137), 45, (255, 253, 231), 'Mairon-KYre.ttf')
                TextWrap(i.description, (30, 200), 30, (255, 253, 231), 'Mairon-KYre.ttf', 30)


        Text(RealmName, (750, 50), 50, (255, 253, 231), 'Mairon-KYre.ttf')


    elif layer == 1:    #Layer 1 (town map)
        Screen.blit(Towns[currentTown].image2, (400,100))
        for i in Towns[currentTown].houseObjects:
            if i.button.DetectClick(mx, my) == True:
                pygame.draw.circle(Screen, (255,0,0), ((i.bounds[0][0]+i.bounds[2][0])/2, (i.bounds[0][1]+i.bounds[2][1])/2), i.dims[1]/2+15, 2)
                Screen.blit(GUI[2], (50,100))
                Text(i.name, (200, 137), 45, (255, 253, 231), 'Mairon-KYre.ttf')
                TextWrap(i.description, (30, 200), 30, (255, 253, 231), 'Mairon-KYre.ttf', 30)
                break

        Text(Towns[currentTown].name, (750, 50), 50, (255, 253, 231), 'Mairon-KYre.ttf')

    elif layer == 2:    #Layer 2 (house map)
        Screen.blit(pygame.image.load('ImageDump/house'+str(currentTown)+'_'+str(currentHouse)+'.png').convert_alpha(), (400,100))
        for x in Towns[currentTown].houseObjects[currentHouse].interactables:
            for y in x:
                if y != 0:
                    Screen.blit(y.image, (410+34*(5-Towns[currentTown].houseObjects[currentHouse].dims[0]/4 + Towns[currentTown].houseObjects[currentHouse].interactables.index(x)), 110+34*(5-Towns[currentTown].houseObjects[currentHouse].dims[1]/4 + x.index(y))) )
                    if mx >= 410+34*(5-Towns[currentTown].houseObjects[currentHouse].dims[0]/4 + Towns[currentTown].houseObjects[currentHouse].interactables.index(x)) and mx < 444+34*(5-Towns[currentTown].houseObjects[currentHouse].dims[0]/4 + Towns[currentTown].houseObjects[currentHouse].interactables.index(x)):
                        if my >= 110+34*(5-Towns[currentTown].houseObjects[currentHouse].dims[1]/4 + x.index(y)) and my < 144+34*(5-Towns[currentTown].houseObjects[currentHouse].dims[1]/4 + x.index(y)):
                            pygame.draw.rect(Screen, (255,0,0), (410+34*(5-Towns[currentTown].houseObjects[currentHouse].dims[0]/4 + Towns[currentTown].houseObjects[currentHouse].interactables.index(x)), 110+34*(5-Towns[currentTown].houseObjects[currentHouse].dims[1]/4 + x.index(y)), 34, 34), 2)
                            Screen.blit(GUI[2], (50,100))
                            Text(y.name, (200, 137), 45, (255, 253, 231), 'Mairon-KYre.ttf')
                            TextWrap(y.description, (30, 200), 30, (255, 253, 231), 'Mairon-KYre.ttf', 30)
                            if len(y.inventory) > 0:
                                TextWrap("Inventory:", (30, 350), 35, (255, 253, 231), 'Mairon-KYre.ttf', 30)
                            for i in y.inventory:
                                TextWrap("- " + str(i[0]) + "x " + str(i[1]), (30, 400 + 30*y.inventory.index(i)), 30, (255, 253, 231), 'Mairon-KYre.ttf', 30)

        Text(Towns[currentTown].houseObjects[currentHouse].name, (750, 50), 50, (255, 253, 231), 'Mairon-KYre.ttf')

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
        elif event.type == pygame.MOUSEBUTTONDOWN:
            if QuitButton.DetectClick(mx, my) == True:
                QuitButton.function()
            elif InfoButton.DetectClick(mx, my) == True:
                InfoButton.function()
            if layer == 0:
                for b in Towns:
                    if b.button.DetectClick(mx, my) == True and b.button.isClickable == True:
                        b.button.function()
                        break
            elif layer == 1:
                for b in Towns[currentTown].houseObjects:
                    if b.button.DetectClick(mx, my) == True and b.button.isClickable == True:
                        b.button.function()
                        break
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_ESCAPE:
                if HelpMenuActive == True:
                    HelpMenuActive = False
                elif layer > 0:
                    if layer == 1:
                        layer = 0
                        for i in Towns:
                            i.button.isClickable = True
                    if layer == 2:
                        layer = 1
                        for i in Towns[currentTown].houseObjects:
                            i.button.isClickable = True

    Screen.blit(GUI[0], (399,99))           #GUI elements
    Screen.blit(GUI[1], (0,0))

    Screen.blit(GUI[3], (525,13))

    Screen.blit(GUI[4], (1050,22))
    if InfoButton.DetectClick(mx, my) == True:
        Screen.blit(GUI[6], (1050,22))

    Screen.blit(GUI[5], (50,750))
    if QuitButton.DetectClick(mx, my) == True:
        Screen.blit(GUI[7], (50,750))
    Text("Quit", (100, 775), 30, (255, 253, 231), 'Mairon-KYre.ttf')

    HelpMenuLayer()

    pygame.display.update()
