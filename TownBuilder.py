import pygame           #This file is what generates the towns, by generating roads and then houses along the roads.
from math import sin, cos, atan2, pi
import PIL
import random as r
from shapely.geometry import LineString, box, Polygon

pygame.init()

Map = pygame.Surface((800,800))

buildings = [       #Images of each type of building
    pygame.image.load("Images/Buildings/Ahouse.png").convert_alpha(),
    pygame.image.load("Images/Buildings/Squarehouse.png").convert_alpha(),
    pygame.image.load("Images/Buildings/Tower.png").convert_alpha(),
    pygame.image.load("Images/Buildings/Tavern.png").convert_alpha(),
    pygame.image.load("Images/Buildings/Blacksmith.png").convert_alpha(),
    pygame.image.load("Images/Buildings/Church.png").convert_alpha()
]
                #Lists that store all the data for assembling a town
lines = []      #Line segments that form roads
points = []     #End points of roads used as reference for path finding
houses = []     #Raw house data

Centre = [10*r.randint(30,50), 10*r.randint(30,50)]     #Random centre point

def CreateMainRoad():         #Creates 4 main roads from the centre to a random on the edge of the screen, where each main road is perpendicular to each other at the centre to avoid cross overs
    for i in range(4):
        P1 = (0,0)
        if i == 0:
            P1 = (10*r.randint(0,80), 0)
            angle = -90
        elif i == 1:
            P1 = (800, 10*r.randint(0,80))
            angle = 0
        elif i == 2:
            P1 = (10*r.randint(0,80), 800)
            angle = 90
        elif i == 3:
            P1 = (0, 10*r.randint(0,80))
            angle = 180
        P = [Centre[0], Centre[1]]
        points.append(P1)
        line = []
        for i in range(150):
            line.append([P[0], P[1]])
            if line[-1][0] > 0 and line[-1][0] < 800 and line[-1][1] > 0 and line[-1][1] < 800:
                turnRemaining = (atan2((P1[1]-P[1])*pi/180, (P1[0]-P[0])*pi/180))*180/pi - angle
                if turnRemaining > 180: turnRemaining = turnRemaining - 360
                elif turnRemaining < -180: turnRemaining = turnRemaining + 360
                if turnRemaining < -1:
                    angle = angle - 1
                elif turnRemaining > 1:
                    angle = angle + 1

                P[0] += int(10*cos(angle*pi/180))
                P[1] += int(10*sin(angle*pi/180))
            else:
                break
        lines.append(line)

def CreateRoad(MR1):           #Creates smaller roads between the main roads
    MR1 = MR1%4
    tooClose = False
    Point = r.randint(int(len(lines[MR1])/6)+1, int(2*len(lines[MR1])/3))
    for line in lines:
        if line != lines[MR1]:
            for i in line:
                if i in lines[MR1]:
                    if abs(lines[MR1].index(lines[MR1][Point]) - lines[MR1].index(i)) <= 10:
                        tooClose = True
                        break
    if tooClose == False:
        P = [lines[MR1][Point][0], lines[MR1][Point][1]]
        if MR1 < 3:
            MR2 = MR1+1
            Point2 = r.randint(int(len(lines[MR2])/6)+1, int(2*len(lines[MR2])/3))
        else:
            MR2 = 0
            Point2 = r.randint(int(len(lines[MR2])/6)+1, int(2*len(lines[MR2])/3))
        P1 = [lines[MR2][Point2][0], lines[MR2][Point2][1]]
        line = []
        if Point + 1 < len(lines[MR1]):
            angle = -90 + (atan2((lines[MR1][Point][1]-lines[MR1][Point+1][1])*pi/180, (lines[MR1][Point][0]-lines[MR1][Point+1][0])*pi/180))*180/pi
        else:
            angle = -90 + (atan2((lines[MR1][Point-1][1]-lines[MR1][Point][1])*pi/180, (lines[MR1][Point-1][0]-lines[MR1][Point][0])*pi/180))*180/pi

        for i in range(150):
            line.append([P[0], P[1]])
            cut = False
            for i in range(len(lines[MR2])):
                if i + 1 < len(lines[MR2]):
                    if len(line) >= 2:
                        if LineString([line[-1], line[-2]]).intersects(LineString([lines[MR2][i], lines[MR2][i+1]])):
                            cut = True
                        elif line[-1][0] < 0 or line[-1][0] > 800 or line[-1][1] < 0 or line[-1][1] > 800:
                            cut = True
            if cut == False:
                turnRemaining = (atan2((P1[1]-P[1])*pi/180, (P1[0]-P[0])*pi/180))*180/pi - angle
                if turnRemaining > 180: turnRemaining = turnRemaining - 360
                elif turnRemaining < -180: turnRemaining = turnRemaining + 360
                if turnRemaining < -2:
                    angle = angle - 2
                elif turnRemaining > 2:
                    angle = angle + 2

                P[0] += int(10*cos(angle*pi/180))
                P[1] += int(10*sin(angle*pi/180))
            else:
                break
        lines.append(line)


def CreateHouse():         #Creates houses along the roads such that the houses are tangent to the roads
    for line in lines:
        num_houses = r.randint(int(len(line)/6), int(2*len(line)/3))
        # for i in range(4,len(line)-4):
        for i in range(num_houses):
            HouseRand = r.randint(0,20)
            dims = (0,0)
            type = 0
            if HouseRand < 18:
                type = 0
                dims = (0,0)
            elif HouseRand == 18 or HouseRand == 19:
                type = 1
                dims = (20,20)
            else:
                type = 2
                dims = (0,20)

            pos = r.randint(4, int(2*len(line)/3))
            pos2 = r.randint(4, int(2*len(line)/3))
            if i + 1 < len(line):
                cut = False
                cut2 = False
                Normal = -90 + (atan2((line[pos][1]-line[pos+1][1]), (line[pos][0]-line[pos+1][0])))*180/pi
                Normal2 = 90 + (atan2((line[pos2][1]-line[pos2+1][1]), (line[pos2][0]-line[pos2+1][0])))*180/pi


                for h in houses:
                    if Polygon([ (line[pos][0] + 20*cos(Normal*pi/180), line[pos][1] + 20*sin(Normal*pi/180)),  (line[pos][0] + (20+dims[0])*cos((Normal+90)*pi/180) + (20)*cos((Normal)*pi/180), line[pos][1] + (20+dims[0])*sin((Normal+90)*pi/180) + (20)*sin((Normal)*pi/180)),  (line[pos][0] + (20+dims[0])*cos((Normal+90)*pi/180) + (40+dims[1])*cos((Normal)*pi/180), line[pos][1] + (40+dims[1])*sin((Normal)*pi/180)+ (20+dims[0])*sin((Normal+90)*pi/180)) ,  (line[pos][0] + (40+dims[1])*cos((Normal)*pi/180), line[pos][1] + (40+dims[1])*sin((Normal)*pi/180))  ]).intersects( Polygon([ h[0], h[1], h[2], h[3] ]) ):
                        cut = True
                        break

                if cut == False:
                    for e in lines:
                        for a in range(len(e)):
                            if a + 1 < len(e):
                                if Polygon([ (line[pos][0] + 20*cos(Normal*pi/180), line[pos][1] + 20*sin(Normal*pi/180)),  (line[pos][0] + (20+dims[0])*cos((Normal+90)*pi/180) + (20)*cos((Normal)*pi/180), line[pos][1] + (20+dims[0])*sin((Normal+90)*pi/180) + (20)*sin((Normal)*pi/180)),  (line[pos][0] + (20+dims[0])*cos((Normal+90)*pi/180) + (40+dims[1])*cos((Normal)*pi/180), line[pos][1] + (40+dims[1])*sin((Normal)*pi/180)+ (20+dims[0])*sin((Normal+90)*pi/180)) ,  (line[pos][0] + (40+dims[1])*cos((Normal)*pi/180), line[pos][1] + (40+dims[1])*sin((Normal)*pi/180))  ]).intersects( LineString([ e[a], e[a + 1] ])):
                                    cut = True
                                    break


                if cut == False:
                    houses.append([ (line[pos][0] + 20*cos(Normal*pi/180), line[pos][1] + 20*sin(Normal*pi/180)),  (line[pos][0] + (20+dims[0])*cos((Normal+90)*pi/180) + (20)*cos((Normal)*pi/180), line[pos][1] + (20+dims[0])*sin((Normal+90)*pi/180) + (20)*sin((Normal)*pi/180)),  (line[pos][0] + (20+dims[0])*cos((Normal+90)*pi/180) + (40+dims[1])*cos((Normal)*pi/180), line[pos][1] + (40+dims[1])*sin((Normal)*pi/180)+ (20+dims[0])*sin((Normal+90)*pi/180)) ,  (line[pos][0] + (40+dims[1])*cos((Normal)*pi/180), line[pos][1] + (40+dims[1])*sin((Normal)*pi/180)), Normal, type, dims  ])

                for h in houses:
                    if Polygon([ (line[pos2][0] + 20*cos(Normal2*pi/180), line[pos2][1] + 20*sin(Normal2*pi/180)),  (line[pos2][0] + (20+dims[0])*cos((Normal2+90)*pi/180) + (20)*cos((Normal2)*pi/180), line[pos2][1] + (20+dims[0])*sin((Normal2+90)*pi/180) + (20)*sin((Normal2)*pi/180)),  (line[pos2][0] + (20+dims[0])*cos((Normal2+90)*pi/180) + (40+dims[1])*cos((Normal2)*pi/180), line[pos2][1] + (40+dims[1])*sin((Normal2)*pi/180)+ (20+dims[0])*sin((Normal2+90)*pi/180)) ,  (line[pos2][0] + (40+dims[1])*cos((Normal2)*pi/180), line[pos2][1] + (40+dims[1])*sin((Normal2)*pi/180)) ]).intersects( Polygon([ h[0], h[1], h[2], h[3] ]) ):
                        cut2 = True
                        break

                if cut2 == False:
                    for e in lines:
                        for a in range(len(e)):
                            if a + 1 < len(e):
                                if Polygon([ (line[pos2][0] + 20*cos(Normal2*pi/180), line[pos2][1] + 20*sin(Normal2*pi/180)),  (line[pos2][0] + (20+dims[0])*cos((Normal2+90)*pi/180) + (20)*cos((Normal2)*pi/180), line[pos2][1] + (20+dims[0])*sin((Normal2+90)*pi/180) + (20)*sin((Normal2)*pi/180)),  (line[pos2][0] + (20+dims[0])*cos((Normal2+90)*pi/180) + (40+dims[1])*cos((Normal2)*pi/180), line[pos2][1] + (40+dims[1])*sin((Normal2)*pi/180)+ (20+dims[0])*sin((Normal2+90)*pi/180)) ,  (line[pos2][0] + (40+dims[1])*cos((Normal2)*pi/180), line[pos2][1] + (40+dims[1])*sin((Normal2)*pi/180))  ]).intersects( LineString([ e[a], e[a + 1] ])):
                                    cut2 = True
                                    break

                if cut2 == False:
                    houses.append([ (line[pos2][0] + 20*cos(Normal2*pi/180), line[pos2][1] + 20*sin(Normal2*pi/180)),  (line[pos2][0] + (20+dims[0])*cos((Normal2+90)*pi/180) + (20)*cos((Normal2)*pi/180), line[pos2][1] + (20+dims[0])*sin((Normal2+90)*pi/180) + (20)*sin((Normal2)*pi/180)),  (line[pos2][0] + (20+dims[0])*cos((Normal2+90)*pi/180) + (40+dims[1])*cos((Normal2)*pi/180), line[pos2][1] + (40+dims[1])*sin((Normal2)*pi/180)+ (20+dims[0])*sin((Normal2+90)*pi/180)) ,  (line[pos2][0] + (40+dims[1])*cos((Normal2)*pi/180), line[pos2][1] + (40+dims[1])*sin((Normal2)*pi/180)), Normal2, type, dims  ])



RoadColours = [         #Colours used for the roads
[146,124,72],
[140,115,66],
[155,122,67]
]

def Render():       #Draws all the roads and houses to a pygame surface
    pygame.draw.circle(Map, (140,115,66), Centre, 20)
    for i in houses:
            if i[5] == 0:
                num = r.randint(0,20)
                HouseImangeNumber = 0
                if num < 10: HouseImangeNumber = 0
                elif num >= 10 and num < 18: HouseImangeNumber = 1
                else: HouseImangeNumber = 2
            elif i[5] == 1:
                num = r.randint(0,1)
                HouseImangeNumber = 3
                if num == 0: HouseImangeNumber = 3
                else: HouseImangeNumber = 4
            else:
                HouseImangeNumber = 5
            HouseImage = pygame.transform.rotate(buildings[HouseImangeNumber], 90-i[4])
            hrect = HouseImage.get_rect()
            Map.blit(HouseImage, ( (i[0][0]+i[1][0])/2-(hrect.width/2) + (10+i[6][0]/2)*cos((i[4])*pi/180) , (i[0][1]+i[1][1])/2-(hrect.height/2) + (10+i[6][1]/2)*sin(i[4]*pi/180) ))
            i.append(HouseImangeNumber)

    for line in lines:
        for i in range(len(line)):
            if i + 1 < len(line):
                if lines.index(line) <= 3:
                    pygame.draw.line(Map, RoadColours[r.randint(0,2)], line[i], line[i+1], 8)
                else:
                    pygame.draw.line(Map, RoadColours[r.randint(0,2)], line[i], line[i+1], 4)


def CreateTown(number):    #Executes all the functions in the correct order and saves the image
    CreateMainRoad()
    for i in range(r.randint(2,6)):
        CreateRoad(i)

    CreateHouse()
    Render()
    pygame.image.save(Map, 'ImageDump/town'+str(number)+'.png')
