# DnD World Builder

## System Specifications

#### Software:
##### **Required**
  - **OS:** MacOS Mojave (version 10.14 used during testing)
  - **Language:** Python 3.7.4
  - **Libraries:** (found in requirements.txt)

#### Hardware:
##### **Minimum**
  - **CPU:** 64 Bit 1.8GHz dual core processor
  - **Memory:** 2GB
  - **GPU:** Intel HD Graphics 6000
  - **Storage:** 7200rpm HDD with at least 100MB free
  - **Network Connection:** stable connection to install libraries

##### **Recommended**
  - **CPU:** 64 Bit 2.5GHz dual core processor
  - **Memory:** 4GB
  - **GPU:** Intel HD Graphics 6000
  - **Storage:** SSD with at least 100MB free
  - **Network Connection:** stable connection to install libraries

## User Manuel

#### Installation & Startup Guide:
  1. Extract compressed file.
  2. Using Terminal or another console, cd into the directory that contains Main.py by running: ```cd <filepath>```
  3. Using the same Terminal window, run: ```pip3 install -r requirements.txt``` to install the required libraries.
  4. Using the same Terminal window, run ```python3 Main.py``` to launch the program.
  5. After about 2 minutes of generating, you will be put into the main screen and can start using the program.

#### GUI Guide
  - Left side of the screen displays information about the selected icon (name of icon at them top).
  - Right side of the screen displays an interactive map of the current location (name of location at them top).
  - All icons and buttons get a bright red outline or circle when the user hovers the mouse over them.
  - In the bottom left of the screen there is a 'Quit' button that closes the program when clicked
  - In the top right of the screen there is an (i) button that gives more information about the current screen when clicked.

  ![Main region map with all screen elements labeled](./ScreenShots/ScreenElements.png)


#### Controls (Simplified):
  - Hover the mouse over icons (towns, buildings & Interactable objects) for more information to be displayed.
  - Click on icons (towns & buildings) to travel to their corresponding location
  - Click on buttons to execute their respective functions
  - Press [ESC] to close pop outs and to return to previous location.

#### How To Use:

**Region Map**
When on the region map screen, the right of the screen is taken up by the region map, which the user can interact with using the mouse. Hovering the mouse over a town (the small groupings of orange dots across the map) will display the name of the town as well as its description on the left of the screen. It also circles the selected house in red. Clicking on a town will take the user to the town map screen of the corresponding town.

![Region map](./ScreenShots/RegionMap.png)

**Town Map**
When on the town map screen, the right of the screen is taken up by the town map, which the user can interact with using the mouse. Hovering the mouse over a house will display the type of house as well as its description on the left of the screen. It also circles the selected house in red. Clicking on a house will take the user to the house map screen of the corresponding house. Pressing the [ESC] key will return the user to the region map.

![Town map](./ScreenShots/TownMap.png)

**House Map**
When on the house map screen, the right of the screen is taken up by the house map, which the user can interact with using the mouse. Hovering the mouse over an interactable object will display the type of object as well as its description on the left of the screen. It also outlines the selected house in red. Some objects have an inventory. These objects have the contents of their inventory listed below their description. Pressing the [ESC] key will return the user to the town map they came from.

![House map](./ScreenShots/HouseMap.png)

**Utilities**
There are 2 utility buttons in this program, and they are both present on all 3 screens. These are the 'Quit' and '(i)' buttons.

The 'Quit' button closed the program when clicked. When the user hovers the mouse over the 'Quit' button, it is outlined in red so that the user knows it can be interacted with.

!['Quit' button selected](./ScreenShots/QuitHighlighted.png)

The '(i)' button (get info button) also is outlined in red when the user hovers the mouse over it. When the user clicks on the '(i)' button, a pop-up appears that gives a summary of what the user can do on the screen they are on. It also points to both the 'Quit' button and an icon in the map section to guide the user in what to look for and how to use the program. It effectively gives a user a simplified version of the controls. The pop-up remains even when the user is no longer hovering over the '(i)' button so that they can click on the marked icon if they need assistance. Pressing the [ESC] key will close the pop-up.

!['(i)' button selected](./ScreenShots/GetInfoPopUp.png)

#### Troubleshooting:
  - If an error causes the program to crash, launch the program again.
  - If an error occurs that doesn't cause the program to crash, close the program using the 'Quit' button and launch it again.

## Test Data

#### TownLocations(Locations):

```python
def TownLocations(Locations):           #Linear search of a 2D array (image) to find ideal tiles to place a town on
    image = Image.open('ImageDump/Map.png').convert('RGB')
    imageArray  = np.array(image)
    xPass = 0
    yPass = 0
    for x in range(32):
        if xPass == 0:
            for y in range(32):
                if yPass == 0:
                    CorrectColours = []
                    for i in range(x*25, (x+1)*25):
                        for j in range(y*25, (y+1)*25):
                            if int(imageArray[i][j][1]) >= 70 and list(imageArray[i][j]) != [160,160,220]:
                                CorrectColours.append((i,j))
                    if len(CorrectColours) >= 594:  #95% (594/625) of the pixels have to be correct for the tile to be suitable
                        Locations.append((25*y,25*x))
                        xPass = 1
                        yPass = 1

                else:
                    yPass = 0
        else:
            xPass = 0
    image.save("ImageDump/Map.png")
```

This is a linear search algorithm in my program that is used to search a map image and output (x, y) locations that are suitable for a town to generate on. This is effectively a nested 2D linear search, as the main 2D linear search uses a secondary 2D linear search to determine if the iteration it is on meets the requirements for a town. It works by comparing the 'green' value of each pixel in a 25x25 pixel tile to see if it is above 70. This means that grass and snow are valid areas, but not water, sand, mountains, or trees (green is too dark). The colour [160,160,220] is the colour of ice sheets, so it is also ignored.

This algorithm uses 2 pieces of outside data to operate; an image named 'Map.png' that is located in the 'ImageDump' directory. This image must be 800x800 pixels. If it is any less in any dimension, the algorithm would crash since it would be searching out of the range of the list. If the image were any larger than 800x800, nothing would happen, it just wouldn't check the pixels out of the 800x800 region in the top left. The algorithm also uses the locations array that is parsed to it as a parameter. If the array is not empty the algorithm wouldn't crash, but it would mean that an incorrect result is produced, as the data that was there originally wouldn't correspond to a town location found by the algorithm.

example of 'Map.png':
![Map](./ImageDump/Map.png)

example of 'Locations':
```python
[]
```

#### LootTable(self):

```python
def LootTable(self):    #Generates loot in the inventory from a loot table
      if self.LootTableName != 0:
          table = open("Text/LootTables/" + self.LootTableName + ".txt", "r")
          for line in table:
              loot = line[:-1].split("|")
              if r.randint(0,100) >= int(loot[2]):
                  amount = r.randint(int(loot[0]), int(loot[1]))
                  if amount > 0:
                      self.inventory.append([amount, loot[3]])
```

This is an algorithm that generates loot for the inventory of an interactable object from a loot table. It works by separating the loot table file (text file) into lines, then splitting the lines at the '|' symbols. The third value on the line is the weight of the item, so for that item to generate at all, a random number between 0 and 100 must exceed the weight value. The first and second values are the lower and upper limits respectively for the amount of the item being generated. The quantity of the item generated is a random number between the lower and upper limits (inclusive). Finally the fourth value is the name of the item. The program then appends the item and its quantity to the inventory of the interactable object.

The outside data required for this algorithm is the object itself, which is an interactable object, as well as the loot table that corresponds to the loot table name of the object. The interactable object must be a valid object generated from the Interactable class. The .LootTableName has to be either 0 or the name of a valid loot table text file. The loot table must be a text file that follows the rules as explained.

Example of 'self':
```python
Interactable( "Barrel", 4, "A storage barrel.", "HouseBarrel" )
```

Example of 'HouseBarrel.txt':
0|20|50|Gold pieces
2|5|0|Loaf of bread
0|2|0|Preserved meat
3|6|0|Rations
0|3|0|Bandage
0|1|0|Mess kit
0|1|50|Dagger

#### TextWrap(text, position, size, frontcolor, textfont, spacing):

```python
def TextWrap(text, position, size, colour, textfont, spacing):      #Draws text on the screen that wraps where the '|' is placed convetionally (top right corner is the position given)
    Lines = text.split('|')
    for line in Lines:
        font = pygame.font.Font(textfont, size)
        text = font.render(line, True, colour)
        textRect = text.get_rect()
        textRect.topleft = (position[0], position[1] + spacing*Lines.index(line))
        Screen.blit(text, textRect)
```

This is a function that draws text on the screen such that it prints it across multiple lines if the text is suitable. It works by splitting the text at '|' symbols and then rendering the text at the desired position, size, colour, font and spacing.

The outside data that this function requires to be tested are:

  - **text:** a string, no requirements, only that it makes a new line wherever a '|' is.

E.G.
```python
"A stone tower that|houses the town's|weapons, tools and|armour."
```
  - **position:** a tuple formatted as (x, y), where both x and y are positive integers within the range of the screen size. Floats will crash it.

E.G.
```python
(30, 200)
```
  - **size:** a positive integer greater than zero. Floats will crash it.

E.G.
```python
30
```
  - **colour:** a tuple formatted as (R, G, B), where R, G and B are positive integers between 0 and 255 (inclusive). Floats will crash it.

E.G.
```python
(255, 253, 231)
```
  - **textfont:** a string that is the file path of a valid font file (.ttf). Anything else will crash it.

E.G.
```python
"Mairon-KYre.ttf"
```
  - **spacing:** a positive integer greater than zero. Floats will crash it.

E.G.
```python
30
```
  - **Screen:** an initialised pygame display. Anything else will crash it except for pygame surfaces, but they don't display to the screen, they have to either be saved separately or bitted to an actual pygame display.

E.G.
```python
pygame.display.set_mode((1150, 850))
```

## Justification of Code

The **Test Data** section contains extra information about how 3 of my algorithms work. These are the TownLocations(Locations) function which is part of Main.py, the LootTable(self) function which is part of the Interactable class in HousePresets.py, and the TextWrap(text, position, size, colour, textfont, spacing) function which is part of Main.py.

#### Main.py

Main.py is the main python file and houses the core functions and classes, as well as the game loop. This file is what imports all the other files to execute specific functions and acquire data.

The functions that are in Main.py are functions that the user would activate when using the program, as well as functions that are relevant to the GUI. This includes functions like ChangeTown(TownNumber), ChangeHouse(TownNumber, HouseNumber) and ToggleHelpMenu() to name a few. When building this program, I put a very big emphasis on modularity. The goal for me was to be able to make modules that are able to create variety in the object, but to keep unnecessary bloat to a minimum. Things like the Button class achieve this well as it allows me to place button anywhere on the screen that can exectute whatever function it wants without a dedicated 'if' statement for it, which in the past has contributed to a large amount of unnecessary code.

One of the classes in Main.py is the Town class:
```python
class Town:     #Town class stores data about the town as well as the button used to navigate to it and lists of data relating to the houses in the town. Has a function to create its own image (draw()) and to make its out description (info())
    def __init__(self, pos, index, name):
        self.pos = pos
        self.index = index
        self.image = 0
        self.image2 = 0
        self.button = Button( [ (self.pos[0]*7/8 + 400,self.pos[1]*7/8 + 100), (self.pos[0]*7/8 + 420,self.pos[1]*7/8 + 100), (self.pos[0]*7/8 + 420,self.pos[1]*7/8 + 120), (self.pos[0]*7/8 + 400,self.pos[1]*7/8 + 120) ] , lambda: ChangeTown(self.index), True)
        self.houses = []
        self.houseObjects = []
        self.name = name
        self.description = ''

    def Draw(self):     #Function used to create an image for the town
        import TownBuilder
        TownBuilder.CreateTown(self.index)
        MapBuilder.GenerateTownRegion(32*Seed[0]+32*self.pos[0], 32*Seed[1]+32*self.pos[1], (400,400), Snow, 'CompletedTown'+str(self.index), 32*self.pos[1], pygame.image.load("ImageDump/town"+str(self.index)+".png").convert_alpha())
        self.houses = TownBuilder.houses
        sys.modules.pop('TownBuilder')
        self.Info()

    def Info(self):     #Function used to generate a description for the town based on how many houses are in it
        self.description = 'A small island town|with a population of|' + str(len(self.houses)*4+r.randint(0,1)) + "."
```
This class stores data about each of the towns that is generated. It is referenced very commonly to display images and data to the screen. These are what its attributes are used for:

  - **pos:** the position the town is located on the region map. Used as reference for displaying it on the region map and for creating the bounding box for its button.
  - **index:** the number of the town, ordered in the order it was generated in. Used so the program knows which town it is using at a given time.
  - **image:** 20x20 icon that is displayed on the region map.
  - **image2:** 700x700 image that is displayed on the town map.
  - **button:** the button object that corresponds to this town on the region map. Clicking on the button will set the current town to the index of the town clicked on, moving the user to the town map of the town clicked on.
  - **houses:** list that gets filled with the raw house data created by the TownBuilder.py file.
  - **houseObjects:** list that gets filled with the house objects as they are generated.
  - **name:** string that is the name of the town that gets displayed.
  - **description:** string that is the description of the town that gets displayed.

The Draw(self) function of the Town object is the function that uses the TownBuilder.py file to create the town and all the house position data. This creates the town image that later gets shrunk down into the images that are displayed on the screen. It also calls the Info(self) function as at that point it has the required data to execute that function.

The Info(self) function of the Town object is the function that creates a description for the town based on the number of houses that the town generated.

The main game loop for this program is separated in 3 sections. At the top there are 3 condition statements, 1 for each layer. These display the main content to the screen, which is everything other than GUI elements.
```python
if layer == 0:  #Layer 0 (region map)
      Screen.blit(pygame.transform.scale(pygame.image.load("ImageDump/Map.png").convert_alpha(), (700,700)), (400,100))
      for i in Towns:
          Screen.blit(i.image, (i.pos[0]*7/8 + 400, i.pos[1]*7/8 + 100))
          if i.button.DetectClick(mx, my) == True:
              pygame.draw.circle(Screen, (255,0,0), (7/8*i.pos[0] + 410, 7/8*i.pos[1] + 110), 15, 2)
              Screen.blit(GUI[2], (50,100))
              Text(i.name, (200, 137), 45, (255, 253, 231), 'Mairon-KYre.ttf')
              TextWrap(i.description, (30, 200), 30, (255, 253, 231), 'Mairon-KYre.ttf', 30)


      Text(RealmName, (750, 50), 50, (255, 253, 231), 'Mairon-KYre.ttf')


  elif layer == 1:    #Layer 1 (town map)
      Screen.blit(Towns[currentTown].image2, (400,100))
      for i in Towns[currentTown].houseObjects:
          if i.button.DetectClick(mx, my) == True:
              pygame.draw.circle(Screen, (255,0,0), ((i.bounds[0][0]+i.bounds[2][0])/2, (i.bounds[0][1]+i.bounds[2][1])/2), i.dims[1]/2+15, 2)
              Screen.blit(GUI[2], (50,100))
              Text(i.name, (200, 137), 45, (255, 253, 231), 'Mairon-KYre.ttf')
              TextWrap(i.description, (30, 200), 30, (255, 253, 231), 'Mairon-KYre.ttf', 30)
              break

      Text(Towns[currentTown].name, (750, 50), 50, (255, 253, 231), 'Mairon-KYre.ttf')

  elif layer == 2:    #Layer 2 (house map)
      Screen.blit(pygame.image.load('ImageDump/house'+str(currentTown)+'_'+str(currentHouse)+'.png').convert_alpha(), (400,100))
      for x in Towns[currentTown].houseObjects[currentHouse].interactables:
          for y in x:
              if y != 0:
                  Screen.blit(y.image, (410+34*(5-Towns[currentTown].houseObjects[currentHouse].dims[0]/4 + Towns[currentTown].houseObjects[currentHouse].interactables.index(x)), 110+34*(5-Towns[currentTown].houseObjects[currentHouse].dims[1]/4 + x.index(y))) )
                  if mx >= 410+34*(5-Towns[currentTown].houseObjects[currentHouse].dims[0]/4 + Towns[currentTown].houseObjects[currentHouse].interactables.index(x)) and mx < 444+34*(5-Towns[currentTown].houseObjects[currentHouse].dims[0]/4 + Towns[currentTown].houseObjects[currentHouse].interactables.index(x)):
                      if my >= 110+34*(5-Towns[currentTown].houseObjects[currentHouse].dims[1]/4 + x.index(y)) and my < 144+34*(5-Towns[currentTown].houseObjects[currentHouse].dims[1]/4 + x.index(y)):
                          pygame.draw.rect(Screen, (255,0,0), (410+34*(5-Towns[currentTown].houseObjects[currentHouse].dims[0]/4 + Towns[currentTown].houseObjects[currentHouse].interactables.index(x)), 110+34*(5-Towns[currentTown].houseObjects[currentHouse].dims[1]/4 + x.index(y)), 34, 34), 2)
                          Screen.blit(GUI[2], (50,100))
                          Text(y.name, (200, 137), 45, (255, 253, 231), 'Mairon-KYre.ttf')
                          TextWrap(y.description, (30, 200), 30, (255, 253, 231), 'Mairon-KYre.ttf', 30)
                          if len(y.inventory) > 0:
                              TextWrap("Inventory:", (30, 350), 35, (255, 253, 231), 'Mairon-KYre.ttf', 30)
                          for i in y.inventory:
                              TextWrap("- " + str(i[0]) + "x " + str(i[1]), (30, 400 + 30*y.inventory.index(i)), 30, (255, 253, 231), 'Mairon-KYre.ttf', 30)

      Text(Towns[currentTown].houseObjects[currentHouse].name, (750, 50), 50, (255, 253, 231), 'Mairon-KYre.ttf')
```
In the middle, there is the event block. This handles all the events like mouse clicks and key strokes and executes functions that correspond to the event.
```python
for event in pygame.event.get():
      if event.type == pygame.QUIT:
          pygame.quit()
      elif event.type == pygame.MOUSEBUTTONDOWN:
          if QuitButton.DetectClick(mx, my) == True:
              QuitButton.function()
          elif InfoButton.DetectClick(mx, my) == True:
              InfoButton.function()
          if layer == 0:
              for b in Towns:
                  if b.button.DetectClick(mx, my) == True and b.button.isClickable == True:
                      b.button.function()
                      break
          elif layer == 1:
              for b in Towns[currentTown].houseObjects:
                  if b.button.DetectClick(mx, my) == True and b.button.isClickable == True:
                      b.button.function()
                      break
      elif event.type == pygame.KEYDOWN:
          if event.key == pygame.K_ESCAPE:
              if HelpMenuActive == True:
                  HelpMenuActive = False
              elif layer > 0:
                  if layer == 1:
                      layer = 0
                      for i in Towns:
                          i.button.isClickable = True
                  if layer == 2:
                      layer = 1
                      for i in Towns[currentTown].houseObjects:
                          i.button.isClickable = True
```
In the bottom there are GUI elements that get blitted over the top of the main content.
```python
Screen.blit(GUI[0], (399,99))           #GUI elements
  Screen.blit(GUI[1], (0,0))

  Screen.blit(GUI[3], (525,13))

  Screen.blit(GUI[4], (1050,22))
  if InfoButton.DetectClick(mx, my) == True:
      Screen.blit(GUI[6], (1050,22))

  Screen.blit(GUI[5], (50,750))
  if QuitButton.DetectClick(mx, my) == True:
      Screen.blit(GUI[7], (50,750))
  Text("Quit", (100, 775), 30, (255, 253, 231), 'Mairon-KYre.ttf')

  HelpMenuLayer()
```

#### MapBuilder.py

MapBuilder.py is a python file that generates the perlin noise map images used for both the region map and the town map. It has 2 functions, 1 for the region map and 1 for the town map. They both work on similar principles, but the main differences between them is that region map generates higher detailed although zoomed out map, versus the zoomed in and lower resolution map of the town map. The town map function as to do more in the ways of calculations, as it has to figure out its relative position on the larger map and add snow accordingly. It also has to ensure it doesn't place its trees off the map, while also placing them so that the bottom middle pixel does not land on a house or road. Though both functions use 2 perlin noise maps to achieve the random placement of trees, where if there is a 'high' point at the same point on both maps (i.e. the high points overlap), then a tree or mountain has a chance to spawn depending on the height. The natural looking colour transitions are created by the perlin noise, where the value (between 0 and 2) is used as a multiplier in an (R,G,B) tuple.

Here is an example of this:
```python
if world[i][j] > 1.22:
          if j + yOffset < 300 + (32)*SnowStart and j + yOffset >= (32)*(SnowStart + 100):
              pygame.draw.circle(TownMap, (0,255*(world[i][j]/2-0.35),75-((j-SnowStart-100)/3 )*3/4), (i,j), 1)
          else:
              pygame.draw.circle(TownMap, (0,255*(world[i][j]/2-0.35),75), (i,j), 1)
          if j + yOffset < (32)*(SnowStart + r.randint(-10, 10)):
              pygame.draw.circle(TownMap, (70 + 200*(world[i][j]/2.5),70 + 200*(world[i][j]/2.5),70 + 255*(world[i][j]/2.5)), (i,j), 2)
          elif j + yOffset >= (32)*(300 + SnowStart):
              pygame.draw.circle(TownMap, (0,255*(world[i][j]/2-0.35),0), (i,j), 1)

```
```python
pygame.draw.circle(TownMap, (0,255*(world[i][j]/2-0.35),75-((j-SnowStart-100)/3 )*3/4), (i,j), 1)
```
This is an example of how the program uses the perin noise value ( world[i][j] ) as part of the (R,G,B) value of the pygame circle. Since the perlin noise map creates a smooth transition of values, this translates into a smooth transition of colours.

#### TownBuilder.py

TownBuilder.py is a python file that is used generate the towns as well as the raw data for the houses in the towns (position, angle, house type, polygon, etc). The roads are made using a path finding algorithm I made, and the houses are generated using an ungodly amount of trigonometry. TownBuilder.py has 5 functions, CreateMainRoad(), CreateRoad(), CreateHouse(), Render() and CreateTown().

CreateMainRoad() makes 4 roads that all stem from a town centre at right angles to each other, and path find to a random point on the edge of the screen. The path finding algorithm works using a starting point, starting angle, ending point, and a turning value. The starting and ending points are pretty self explanatory. The starting angle is important as that is used to determine what direction to turn to in order to face more towards the ending point. The angle that the point is at also determines where the next point. Effectively the algorithm works by creating a bunch of points using the algorithm that path towards the ending point, then connecting the points with lines to create the roads.

CreateRoad() is very similar to CreateMainRoad() except it is called a random number of times to create a random number of smaller roads that connect the main roads. It uses the same algorithm, except the starting point is a random point on a main road, its starting angle is the normal (90 degrees) to the main road, and the ending point is a random point on the next main road over. This ends up creating a spider-web-like pattern for the roads.

This is the main algorithm for creating the roads:
```python
turnRemaining = (atan2((P1[1]-P[1])*pi/180, (P1[0]-P[0])*pi/180))*180/pi - angle
if turnRemaining > 180: turnRemaining = turnRemaining - 360
elif turnRemaining < -180: turnRemaining = turnRemaining + 360
if turnRemaining < -2:
  angle = angle - 2
elif turnRemaining > 2:
  angle = angle + 2

P[0] += int(10*cos(angle*pi/180))
P[1] += int(10*sin(angle*pi/180))
```
where P and P1 represent the current and final points

CreateHouse() effectively places houses randomly along roads such that they don't overlap other houses or other roads. It needs to find the polygon (all 4 corners) that the house will be represented by. But the polygon has to be tangent (in line with) the road, while also being projected a fixed distance away from (normal to) the road.

This is the python trigonometry for this:
```python
Polygon([ (line[pos][0] + 20*cos(Normal*pi/180), line[pos][1] + 20*sin(Normal*pi/180)),  (line[pos][0] + (20+dims[0])*cos((Normal+90)*pi/180) + (20)*cos((Normal)*pi/180), line[pos][1] + (20+dims[0])*sin((Normal+90)*pi/180) + (20)*sin((Normal)*pi/180)),  (line[pos][0] + (20+dims[0])*cos((Normal+90)*pi/180) + (40+dims[1])*cos((Normal)*pi/180), line[pos][1] + (40+dims[1])*sin((Normal)*pi/180)+ (20+dims[0])*sin((Normal+90)*pi/180)) ,  (line[pos][0] + (40+dims[1])*cos((Normal)*pi/180), line[pos][1] + (40+dims[1])*sin((Normal)*pi/180))  ])
```
And this is a visualisation of this using desmos:
![Desmos demo map](./ScreenShots/Desmos.png)

Render() is very simple, it takes all the raw data from the last 3 functions and combines them into and image.

CreateTown() is also very simple, it just executes these in the correct order in one function so the TownBuilder.py file only needs to be referenced the 1 time.

#### HouseBuilder.py

HouseBuilder.py is a python file that makes the floorplans for the buildings. It has 2 functions that do things; DrawGrid(HouseType, HouseImageNumber) and DrawWalls(HouseImageNumber, GridSize), as well a GenerateAll(HouseType, HouseImageNumber, TownNumber, HouseNumber) function that just executes the other 2 functions and saves the result as an image.

DrawGrid(HouseType, HouseImageNumber) draws a grid (10x10, 20x20, or 10x20 depending on the house) in the shape of the house it is assigned, which it determines from the HouseType parameter. It also renders the floor texture, which it determines based on the HouseImageNumber parameter. This grid is useful for DnD, where a 1" grid is used for moving heros and monsters alike. This grid is also used as a visual indicator for where the interactable objects are located when they are generated in HousePresets.py

This is the DrawGrid(HouseType, HouseImageNumber) function:
```python
def DrawGrid(HouseType, HouseImageNumber):  #This draws a grid based on the size of the house. Grid is for ease of use for DnD
    if HouseType == 0:
        GridSize = (10,10)
    elif HouseType == 1:
        GridSize = (20,20)
    elif HouseType == 2:
        GridSize = (10,20)

    for x in range(GridSize[0]):
        for y in range(GridSize[1]):
            if HouseType == 0 and HouseImageNumber != 2:
                Map.blit(pygame.image.load("Images/Other/Dirt.png"), ( 10 + 34*(10 - GridSize[0]/2 + x), 10 + 34*(10 - GridSize[1]/2 + y) ) )
            else:
                Map.blit(pygame.image.load("Images/Other/Planks.png"), ( 10 + 34*(10 - GridSize[0]/2 + x), 10 + 34*(10 - GridSize[1]/2 + y) ) )

    if HouseImageNumber == 4:
        for x in range(10):
            for y in range(10):
                Map.blit(pygame.image.load("Images/Other/Dirt.png"), ( 10 + 34*(20 - GridSize[0]/2 + x), 10 + 34*(10 - GridSize[1]/2 + y) ) )


    for collumn in range(GridSize[0] + 1):
        pygame.draw.line(Map,(100,100,100),( 10 + 34*(10 - GridSize[0]/2 + collumn), 10 + 34*(10 - GridSize[1]/2) ), ( 10 + 34*(10 - GridSize[0]/2 + collumn), 10 + 34*(10 + GridSize[1]/2) ), 1 )

    for row in range(GridSize[1] + 1):
        pygame.draw.line(Map,(100,100,100),( 10 + 34*(10 - GridSize[0]/2), 10 + 34*(10 - GridSize[1]/2 + row) ), ( 10 + 34*(10 + GridSize[0]/2), 10 + 34*(10 - GridSize[1]/2 + row) ), 1 )

    if HouseType == 1:
        pygame.draw.rect(Map,(0,0,0), ( 350, 350 , 340, 340 ) )

    return GridSize
```
At the top it places the floor tiles, and below that is where it renders the grid to overlay the ground texture. It returns the GridSize so that it can be used for the DrawWalls(HouseImageNumber, GridSize) function without having to repeat code.

The DrawWalls(HouseImageNumber, GridSize) just draws thicker lines to represent walls on the outer edges of each building. It also draws a separator wall on the blacksmith building to separate the main house from the workshop.

#### HousePresets.py

HousePresets.py is a python file stores functions used for populating the houses with interactable objects. I am using presets as an AI solution is too far beyond the scope of the project, but I made sure that the presets still offer variety in the interiors of the houses so that they don't all just feel the same. The only preset that doesn't integrate randomness into it is the church preset, as churches tend to be more organised and consistent.
```python
def ChurchPreset(interactables):    #Preset used for the church
    interactables[4][19] = Interactable( "Door", 1, 'A simple wooden|door.', 0)
    interactables[5][19] = Interactable( "Door", 3, 'A simple wooden|door.', 0 )

    for i in range(7):
        interactables[0][4+2*i] = Interactable( "Pew", 13, "A church pew.", 0 )
        interactables[1][4+2*i] = Interactable( "Pew", 14, "A church pew.", 0 )
        interactables[2][4+2*i] = Interactable( "Pew", 14, "A church pew.", 0 )
        interactables[3][4+2*i] = Interactable( "Pew", 15, "A church pew.", 0 )

        interactables[6][4+2*i] = Interactable( "Pew", 13, "A church pew.", 0 )
        interactables[7][4+2*i] = Interactable( "Pew", 14, "A church pew.", 0 )
        interactables[8][4+2*i] = Interactable( "Pew", 14, "A church pew.", 0 )
        interactables[9][4+2*i] = Interactable( "Pew", 15, "A church pew.", 0 )

    interactables[2][1] = Interactable( "Lectern", 16, "A church lectern.", "Lectern" )

    interactables[4][1] = Interactable( "Altar", 17, "The altar.", "Altar" )
    interactables[5][1] = Interactable( "Altar", 18, "The altar.", "Altar" )

    interactables[7][0] = Interactable( "Organ", 19, "Church organ.", 0 )
    interactables[8][0] = Interactable( "Organ", 20, "Church organ.", 0 )
```
All the other presets are more or less like this one, except random numbers are used for the placement and quantity of objects around the building.

This is the Interactable class:
```python
class Interactable:     #Interactable class stores data about itself such as its image and a loot table
    def __init__(self, name, ID, description, LootTableName):
        self.name = name
        self.description = description
        self.ID = ID
        self.LootTableName = LootTableName
        self.image = pygame.image.load("Images/Interactables/"+str(ID)+".png").convert_alpha()
        self.inventory = []
        self.LootTable()

    def LootTable(self):    #Generates loot in the inventory from a loot table
        if self.LootTableName != 0:
            table = open("Text/LootTables/" + self.LootTableName + ".txt", "r")
            for line in table:
                loot = line[:-1].split("|")
                if r.randint(0,100) >= int(loot[2]):
                    amount = r.randint(int(loot[0]), int(loot[1]))
                    if amount > 0:
                        self.inventory.append([amount, loot[3]])
```
I have already explained how the LootTable(self) function works in the **Test Data** section, but here is a desription of what each of the attributes are and are used for:

  - **name:** a string that is the name of the interactable object. This is displayed on screen.
  - **description:** a string that is the description of the interactable object. This is displayed on screen.
  - **ID:** an integer that is the ID number of the interactable object. This is used to reference what image corresponds to the interactable object.
  - **LootTableName:** either 0 or a string that is the name of the loot table that the object uses to fill its inventory.
  - **image:** the image that corresponds to the ID of the interactable object. This is displayed on screen in the house map screen.
  - **inventory:** an array of lists that contains an integer (quantity) and a string (item name). These get displayed on screen as the loot that can be found in the interactable object.

## Peer Report Desk check

I am desk checking Mr Howse's hangman code:
```Python
import random
dictionary = ["cheese", "bratwurst", "coffee", "elephant", "banana", "steve"]
guess_word = random.choice(dictionary)

display_text = ""                                       # stores the display text
guessed_letter_array = []                               # stores the correctly guessed letters

lives = 7

while display_text != guess_word:
	guessed_letter = input("What letter do you guess? ")
	if guessed_letter in guess_word:
		guessed_letter_array.append(guessed_letter)     # store every correct guess
	else:
		print("Nope, none of that...")
		lives = lives - 1		                        # store the level of suck
		print ("Lives left: ",lives)	                # tell the player they suck
	display_text = ""
	for l in guess_word:                                # rebuild the display text
		if l in guessed_letter_array:                   # show correct guesses
			display_text = display_text + l
		else:
			display_text = display_text + "-"	        # show place holders
	if display_text == guess_word:
		print("Yay! You got it!!!")                     # try to hide your contempt here...
	else:
		print(display_text)
	if lives <= 0:                                      # if they lose, pretend to be surprised
		print("Sorry, you have lost...")
		break                                           # end the loop due to suckiness
```

![Page 1](./Part_B_Desk_Check/Page1.png)

![Page 2](./Part_B_Desk_Check/Page2.png)

![Page 3](./Part_B_Desk_Check/Page3.png)

![Page 4](./Part_B_Desk_Check/Page4.png)

## Reflection

This major work was a great success in some areas but also fell short in others. I didn't get to add all the features I wanted to, but the features I did add met my requirements very well. The main features I didn't get time to include were the Ruins, saving and loading from a file, multiple themes, and NPCs. I feel that this is in part due to me not prototyping any of those features beforehand, as well as not prototyping some of the features that did make the cut. For example, I didn't prototype the house generation process, so I dived into that part of the project effectively blind, so it was not as fast to make the final version as it was for the Region generator and Town generator. Since the houses were an essential part of this project, as they were an extension down from the town map and arguably one of the main reasons I decided to do this project, it was crucial that I got them up to scratch. This ate up a lot of my time and is likely the major contributor to why I wasn't able to get all the features done. During part 1 of the major work, I put a lot of time into the Region generator prototype. This did pay out for me as I had very few changes to make to it for the part B of the major work, but I would say I overdid the prototyping for that one, as I had already gotten it good enough well before I stopped working on it, and I think that if I spent that time prototyping the House generator, then I would have likely completed the House generator faster and had time to work on saving and loading from a file. I am not too fussed at where I got to in the time allocated to this, as I had already ruled out the multiple themes and NPCs features towards the end of my prototyping as they were either too challenging for the time limit, or unnecessary when compared to all the other features.

I would say that the main features of my program are the Region generator, Town generator, House Generator, interactive UI, and the internal help feature. These are the main things that the user gets to see and experience and I think that I have met my requirements very well in these cases. This is particularly evident when comparing the GUI to the screen designs from part 1.

**Region Map:**
![Screen design 2](./Theory_Part_1/ScreenDesign2.png)
![Region Map](./ScreenShots/RegionMap.png)

**Town Map:**
![Screen design 3](./Theory_Part_1/ScreenDesign3.png)
![Town Map](./ScreenShots/TownMap.png)

**House Map:**
![Screen design 4](./Theory_Part_1/ScreenDesign4.png)
![House Map](./ScreenShots/HouseMap.png)

The interactive UI works just like how I planned it in part 1, where you can click on towns and then house for different details specific to that town or house. And though the internal help feature wasn't something I initially thought of when planning in part 1, it came out smooth, updating itself depending on what screen you are on and pointing to key features for the user to interact with.

When it comes to things I did well, the features that I did include I believe I did quite well with. The UI is consistent, the controls are simple and intuitive, and the graphics look good. This has definitely re-affirmed the importance of planning and prototyping for me. Planning things like the screen design as well as the user experience gave me a very clear view of what the end product would look like, so I never found myself struggling with the UI and the overall feel of the program, as that was already cemented in my head. Prototyping was a big thing for me, it really helped in the areas that I did prototype. For example, the TownBuilder.py file was almost completely done after the prototyping, with only one major change being made to it, being that I fixed the maths for placing houses. Having prototypes meant that all the work of finding solutions and directions to take the project in were done at the start, paving the way for me to spend more time polishing and integrating rather than hitting a constant barrage of roadblocks. So the main thing I am taking from my successes is that planning and prototyping really work, and helps eliminate burnouts and de-motivation that comes with building from scratch and experiencing all the unforeseen roadblocks that come with it. Another thing that I think i did well with this project was modular programming. Almost everything is in a function or object, which has removed a lot of potential clutter that would have plagued my code a year or 2 ago. The fact that this program is so self-contained makes it much more stable as well. In my blackjack program for example, since everything that happens is a reaction to the user's input, it has a much higher potential to be broken or behave strangely, and since there were so many possibilities and arrangements for the game to deal with, it was a nightmare to find and fix bugs. But since the only user input in this project is the navigation of the already generated world, there are much fewer points of failure, making it much more stable.

When it comes to areas I can improve on, it would mostly have to do with organisation and time management. I didn't really stick to my gantt chart, which meant I was mainly fixing bugs and adding features as they popped up rather than in a structured order. I also could have managed my time better, as time is the reason why I didn't get to add all the features I wanted to. Time management and organisation go hand in hand, so you stay organised, you likely have also managed your time better, but the opposite is also true. I found that not sticking to the sequencing of events as much as I should have did eat away at my time, as sometimes I would jump back and forth between different areas of the project, wasting time in the process. This was most evident when I was merging the major processes together. Trying to get the Region generator and the Town generator to work together, though it wasn't painful due to modular programming, it did involve me having to go back and forth resolving incompatibility issues and smoothing out the areas where they communicated. Similar issues were also raised when merging the House generator with the Town generator. In terms of roadblocks I experienced, this project was pretty smooth, though trying to get the Region generator and the subsequent loading times to speed up was a constant challenge for me, with nothing I tried being able to work, as the noise library wasn't compatible with the compiler, making compilation a bust, the threading not working either, as the program wasn't waiting for itself and having tens of thousands of threads open at once wasn't improving the speed, and with noise being an algorithm entirely based on starting conditions, caching didn't do much either as it never needed to load data from previous iterations of the loop. It was definitely a very challenging problem that at this stage can't really be helped. The loading times are definitely an inconvenience rather than a bug or something, but I would rather have all the main loading to occur at the start rather than having slowdowns constantly throughout the program whenever you change town, as that would definitely detract from the user experience.

I am very happy with my final product, and I think that it would be good to revisit this is the future to complete it or add more features. I don't think that this could really be used just by itself for a campaign at its current stage of completeness, but it is definitely good for one-offs and for making small towns. That is the thing that most excites me about this program, is that every single building has something in it, not just major areas and buildings strictly a part the campaign. Outside of the requirements I set out for myself in part 1, I would really like to see this program have tools to analyse terrain features and build towns that better integrate into them, or maybe have multi-room and multi-story buildings, so adventurers can explore something like a castle. The major work project was definitely a great experience as is probably my favourite project so far.
