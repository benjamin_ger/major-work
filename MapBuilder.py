import noise            #This file is used to generate all the noise maps used in the program
import numpy as np
import pygame
import random as r

pygame.init()

Map = pygame.Surface((800,800))


Mountains = [   #Mountain images for region map
    pygame.image.load("Images/Mountains/1.png"),
    pygame.image.load("Images/Mountains/2.png"),
    pygame.image.load("Images/Mountains/3.png"),
    pygame.image.load("Images/Mountains/4.png")
    ]

Trees = [       #Tree images for both region and town maps
    pygame.image.load("Images/Trees/MapTrees/1.png"),
    pygame.image.load("Images/Trees/MapTrees/2.png"),
    pygame.image.load("Images/Trees/MapTrees/3.png"),
    pygame.image.load("Images/Trees/MapTrees/4.png"),
    pygame.image.load("Images/Trees/1.png"),
    pygame.image.load("Images/Trees/2.png")
    ]

def GenerateRegion(SeedX, SeedY, size, SnowStart, Name):    #Generates the region map using 2 perlin noise maps to create the terrain and the overlap to place trees, mountains and ice sheets.
    world = np.zeros(size)
    world2 = np.zeros(size)
    for i in range(size[0]):
        for j in range(size[1]):
            world[i][j] = (noise.pnoise2((i+SeedX)/100,
                                        (j+SeedY)/100,
                                        octaves=20,
                                        persistence=0.3,
                                        lacunarity=4,
                                        repeatx=8000,
                                        repeaty=8000,
                                        base=0) + 1)
            world2[i][j] = (noise.pnoise2((i+SeedX+10000)/100,
                                         (j+SeedY+10000)/100,
                                        octaves=20,
                                        persistence=0.3,
                                        lacunarity=4,
                                        repeatx=8000,
                                        repeaty=8000,
                                        base=0) + 1)

            if world[i][j] <= 1.2:
                pygame.draw.circle(Map, (0,0,75+255*world[i][j]/3), (i,j), 1)
            elif world[i][j] > 1.2 and world[i][j] <= 1.22:
                if j < SnowStart + r.randint(-10, 10):
                    pygame.draw.circle(Map, (70 + 200*(world[i][j]/2.5),70 + 200*(world[i][j]/2.5),70 + 255*(world[i][j]/2.5)), (i,j), 1)
                else:
                    pygame.draw.circle(Map, (245*world[i][j]/2,245*world[i][j]/2,170*world[i][j]/2), (i,j), 1)
            elif world[i][j] > 1.22:
                if j < 300 + SnowStart and j >= SnowStart + 100:
                    pygame.draw.circle(Map, (0,255*(world[i][j]/2-0.35),75-((j-SnowStart-100)/3 )*3/4), (i,j), 1)
                else:
                    pygame.draw.circle(Map, (0,255*(world[i][j]/2-0.35),75), (i,j), 1)
                if j < SnowStart + r.randint(-10, 10):
                    pygame.draw.circle(Map, (70 + 200*(world[i][j]/2.5),70 + 200*(world[i][j]/2.5),70 + 255*(world[i][j]/2.5)), (i,j), 1)
                elif j >= 300 + SnowStart:
                    pygame.draw.circle(Map, (0,255*(world[i][j]/2-0.35),0), (i,j), 1)

    for j in range(size[1]):
        for i in range(size[0]):
            if world2[i][j] > 1 and world[i][j] > 1.22 and world[i][j] <= 1.31 and r.randint(1,10) == 2:
                Map.blit(Trees[r.randint(0,3)], (i-4, j-7 ))
            elif world2[i][j] > 1.33 and world[i][j] > 1.35 and r.randint(1,10) == 2:
                Map.blit(Mountains[r.randint(0,3)], (i-4, j-7 ))
            elif world2[i][j] > 1.1 and world[i][j] < 1.19 and j < SnowStart + r.randint(-50, 0) and r.randint(0,10) > 6:
                pygame.draw.circle(Map, (160,160,220), (i,j), 1)

    pygame.image.save(Map, 'ImageDump/'+Name+'.png')

def GenerateTownRegion(SeedX, SeedY, size, SnowStart, Name, yOffset, townImage):    #Generates the town terrain map using 2 perlin noise maps to create the terrain and the overlap to place trees and ice sheets.
    TownMap = pygame.Surface((400,400))
    world = np.zeros(size)
    world2 = np.zeros(size)
    for i in range(size[0]):
        for j in range(size[1]):
            world[i][j] = (noise.pnoise2((i+SeedX)/3200,
                                        (j+SeedY)/3200,
                                        octaves=20,
                                        persistence=0.3,
                                        lacunarity=4,
                                        repeatx=8000,
                                        repeaty=8000,
                                        base=0) + 1)
            world2[i][j] = (noise.pnoise2((i+SeedX+10000)/3200,
                                         (j+SeedY+10000)/3200,
                                        octaves=20,
                                        persistence=0.3,
                                        lacunarity=4,
                                        repeatx=8000,
                                        repeaty=8000,
                                        base=0) + 1)

            if world[i][j] <= 1.2:
                pygame.draw.circle(TownMap, (0,0,75+255*world[i][j]/3), (i,j), 1)
            elif world[i][j] > 1.2 and world[i][j] <= 1.22:
                if j + yOffset < (32)*(SnowStart + r.randint(-10, 10)):
                    pygame.draw.circle(TownMap, (70 + 200*(world[i][j]/2.5),70 + 200*(world[i][j]/2.5),70 + 255*(world[i][j]/2.5)), (i,j), 1)
                else:
                    pygame.draw.circle(TownMap, (245*world[i][j]/2,245*world[i][j]/2,170*world[i][j]/2), (i,j), 1)
            elif world[i][j] > 1.22:
                if j + yOffset < 300 + (32)*SnowStart and j + yOffset >= (32)*(SnowStart + 100):
                    pygame.draw.circle(TownMap, (0,255*(world[i][j]/2-0.35),75-((j-SnowStart-100)/3 )*3/4), (i,j), 1)
                else:
                    pygame.draw.circle(TownMap, (0,255*(world[i][j]/2-0.35),75), (i,j), 1)
                if j + yOffset < (32)*(SnowStart + r.randint(-10, 10)):
                    pygame.draw.circle(TownMap, (70 + 200*(world[i][j]/2.5),70 + 200*(world[i][j]/2.5),70 + 255*(world[i][j]/2.5)), (i,j), 2)
                elif j + yOffset >= (32)*(300 + SnowStart):
                    pygame.draw.circle(TownMap, (0,255*(world[i][j]/2-0.35),0), (i,j), 1)

    TownMap.blit(pygame.transform.scale(townImage, (300,300)), (50,50))
    for j in range(size[1]):
        for i in range(size[0]):
            if world2[i][j] > 1 and world[i][j] > 1.22 and world[i][j] <= 1.31 and r.randint(1,800) == 2 and i >= 17 and j >= 28:
                if TownMap.get_at((i-17, j-28))[:3][0] == 0:
                    TownMap.blit(Trees[r.randint(4,5)], (i-17, j-35 ))
            elif world2[i][j] > 1.1 and world[i][j] < 1.19 and j + yOffset < (32)*(SnowStart + r.randint(-50, 0)) and r.randint(0,10) > 6:
                pygame.draw.circle(TownMap, (160,160,220), (i,j), 1)
    pygame.image.save(TownMap, 'ImageDump/'+Name+'.png')
