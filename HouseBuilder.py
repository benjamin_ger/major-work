import pygame           #This file builds the floorplans for each house that will later be populated with interactable objects

pygame.init()

Map = pygame.Surface((700,700))
# Map = pygame.display.set_mode((700, 700))
Map.fill((0,0,0))


def DrawGrid(HouseType, HouseImageNumber):  #This draws a grid based on the size of the house. Grid is for ease of use for DnD
    if HouseType == 0:
        GridSize = (10,10)
    elif HouseType == 1:
        GridSize = (20,20)
    elif HouseType == 2:
        GridSize = (10,20)

    for x in range(GridSize[0]):
        for y in range(GridSize[1]):
            if HouseType == 0 and HouseImageNumber != 2:
                Map.blit(pygame.image.load("Images/Other/Dirt.png"), ( 10 + 34*(10 - GridSize[0]/2 + x), 10 + 34*(10 - GridSize[1]/2 + y) ) )
            else:
                Map.blit(pygame.image.load("Images/Other/Planks.png"), ( 10 + 34*(10 - GridSize[0]/2 + x), 10 + 34*(10 - GridSize[1]/2 + y) ) )

    if HouseImageNumber == 4:
        for x in range(10):
            for y in range(10):
                Map.blit(pygame.image.load("Images/Other/Dirt.png"), ( 10 + 34*(20 - GridSize[0]/2 + x), 10 + 34*(10 - GridSize[1]/2 + y) ) )


    for collumn in range(GridSize[0] + 1):
        pygame.draw.line(Map,(100,100,100),( 10 + 34*(10 - GridSize[0]/2 + collumn), 10 + 34*(10 - GridSize[1]/2) ), ( 10 + 34*(10 - GridSize[0]/2 + collumn), 10 + 34*(10 + GridSize[1]/2) ), 1 )

    for row in range(GridSize[1] + 1):
        pygame.draw.line(Map,(100,100,100),( 10 + 34*(10 - GridSize[0]/2), 10 + 34*(10 - GridSize[1]/2 + row) ), ( 10 + 34*(10 + GridSize[0]/2), 10 + 34*(10 - GridSize[1]/2 + row) ), 1 )

    if HouseType == 1:
        pygame.draw.rect(Map,(0,0,0), ( 350, 350 , 340, 340 ) )

    return GridSize


def DrawWalls(HouseImageNumber, GridSize):      #This draws the main walls of the house depending on the type of house.
    if HouseImageNumber in [0,1,2]:
        for i in range(2):
            pygame.draw.line(Map,(255, 253, 231),( 10 + 34*(10 - GridSize[0]/2 + GridSize[0]*i), 10 + 34*(10 - GridSize[1]/2) ), ( 10 + 34*(10 - GridSize[0]/2 + GridSize[0]*i), 10 + 34*(10 + GridSize[1]/2) ), 3 )
            pygame.draw.line(Map,(255, 253, 231),( 10 + 34*(10 - GridSize[0]/2), 10 + 34*(10 - GridSize[1]/2 + GridSize[1]*i) ), ( 10 + 34*(10 + GridSize[0]/2), 10 + 34*(10 - GridSize[1]/2 + GridSize[1]*i) ), 3 )
    elif HouseImageNumber in [3,4]:
        pygame.draw.line(Map,(255, 253, 231),( 10 + 34*(10 - GridSize[0]/2), 10 + 34*(10 - GridSize[1]/2) ), ( 10 + 34*(10 - GridSize[0]/2), 10 + 34*(10 + GridSize[1]/2) ), 3 )
        pygame.draw.line(Map,(255, 253, 231),( 10 + 34*(10 - GridSize[0]/2), 10 + 34*(10 - GridSize[1]/2) ), ( 10 + 34*(10 + GridSize[0]/2), 10 + 34*(10 - GridSize[1]/2) ), 3 )

        pygame.draw.line(Map,(255, 253, 231),( 10 + 34*(10 + GridSize[0]/2), 10 + 34*(10 - GridSize[1]/2) ), ( 10 + 34*(10 + GridSize[0]/2), 10 + 34*(10) ), 3 )
        pygame.draw.line(Map,(255, 253, 231),( 10 + 34*(10), 10 + 34*(10) ), ( 10 + 34*(10), 10 + 34*(10 + GridSize[1]/2) ), 3 )

        pygame.draw.line(Map,(255, 253, 231),( 10 + 34*(10), 10 + 34*(10) ), ( 10 + 34*(10 + GridSize[0]/2), 10 + 34*(10) ), 3 )
        pygame.draw.line(Map,(255, 253, 231),( 10 + 34*(10-GridSize[0]/2), 10 + 34*(10 + GridSize[1]/2) ), ( 10 + 34*(10), 10 + 34*(10 + GridSize[1]/2) ), 3 )

        if HouseImageNumber == 4:
            pygame.draw.line(Map,(255, 253, 231),( 10 + 34*(10), 10 + 34*(10 - GridSize[1]/2) ), ( 10 + 34*(10), 10 + 34*(10) ), 3 )
    elif HouseImageNumber == 5:
        for i in range(2):
            pygame.draw.line(Map,(255, 253, 231),( 10 + 34*(10 - GridSize[0]/2 + GridSize[0]*i), 10 + 34*(10 - GridSize[1]/2) ), ( 10 + 34*(10 - GridSize[0]/2 + GridSize[0]*i), 10 + 34*(10 + GridSize[1]/2) ), 3 )
            pygame.draw.line(Map,(255, 253, 231),( 10 + 34*(10 - GridSize[0]/2), 10 + 34*(10 - GridSize[1]/2 + GridSize[1]*i) ), ( 10 + 34*(10 + GridSize[0]/2), 10 + 34*(10 - GridSize[1]/2 + GridSize[1]*i) ), 3 )

def GenerateAll(HouseType, HouseImageNumber, TownNumber, HouseNumber):      #This creates an image that is used for the floorplan
    DrawWalls(HouseImageNumber, DrawGrid(HouseType, HouseImageNumber))
    pygame.image.save(Map, 'ImageDump/house'+str(TownNumber)+'_'+str(HouseNumber)+'.png')
