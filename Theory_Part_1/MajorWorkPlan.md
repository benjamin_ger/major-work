# DnD World Builder

I will be using python with libraries like pygame and pillow to create a tool that builds DnD worlds for use in campaigns. Depending on the scope of the project, I may or may not add campaign structures along with the world.

Worlds will consist of terrain and structures. Cities will be modular, being made of house or structure modules. Each module can store a loot table, so that random loot can be found in different areas, and loot is dependant on the module it came from. (eg sword from blacksmith and bread from farmhouse). Likewise, NPCs and enemies will also be stored in the modules.

Worlds will have a theme, and this theme will dictate the end goal of the campaign. The journey of the campaign will depend on the world and how it is laid out.

Dungeondraft is an option for creating the visuals, at least some of them. The actual terrain may need to be made procedurally. Having an algorithm to denote biomes on the map will help when it comes to the placement of structures. (eg, ensures docks/harbours are on the beach, cities are not in deserts).

Campaign structure will not utilise the entire map. This is to allow DMs to add or subtract from the campaign manually or as they please to suit their needs accordingly.

Areas of the map will also be separated by level, this means that a level 3 module will contain tougher enemies than a level 1 module.

Modules will be categorised, so that the program selects the correct type of module for a given theme.

Themes may introduce new rules to play by. (eg in Archipelagos, there would be rules on how to sail a ship.)

Theme Ideas:
- Fantasy
- Hell-scape
- Archipelagos
- Tundra
- Aether/Sky

Structure Ideas:
- blacksmith
- bakery
- mill
- keep
- house
- farmhouse
- ruins
- church
