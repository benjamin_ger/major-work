# Needs
Features that the program will need to have to be successful.
- **Bird's eye view of the entire generated world:** The user needs to be able to see the entire map, so that the program will be useful in DnD sessions when showing the players the map, where they are and where they are going. Having a full view of the map is a necessary feature to most DMs.
- **Bird's eye view of individual structure floor-plans:** The User needs to be able to click on structures to zoom into the floor-plan. This is extremely necessary to the DM as they need to be able to show the players the room they are in as well as what is in it.
- **Program must be able to organise cities together:** The program must be able to organise structures into cities as cities are a prominent feature of most worlds. Cities are also a great place for players to rest, restock supplies, and to act as a checkpoint in many campaigns.
- **Program must be able to use procedural generation:** The program needs to be able to build natural looking terrain, something that procedural generation lends itself to.
- **Stuff to fill the rooms:** The program needs to have loot, enemies and other interactable features in each structure so that the rooms are not just empty.
- **GUI:** The GUI must be easy understandable, such that the DM can use the program to show the players where they are and so that the players can understand it.
- **Variety of assets:** The program must have a variety of image and text assets so that it has a larger pool of resources to draw from when randomly generating. This means there are more options and therefore more combinations, making each world different.
- **Saving and loading:** The program must be able to save a world and load from memory. This is because often DnD campaigns span over multiple sessions, and if the program was closed and saving and loading were not implemented, the world would be lost.

# Objectives
Features that will hopefully make it into the program but are not necessary for success.
- **Bug free:** It is ideal that the program is bug free so that the user has a consistent and stable experience using it.
- **Zooming and panning:** I would like to be able to zoom in and out and pan around the map. This feature will be useful to the DM but not entirely necessary.
- **Developed GUI:** The GUI must be easy to understand, but I would like to develop it to look good and be more detailed. This will benefit both the DM and the players.
- **Structure editing:** Edit the position and contents of interactable structures within each individual structure. This allows the DM to move traps, doors, loot and more, as well as to add and remove thing, giving the DM a higher level of control over the map, allowing them to tailor the map to fit with the campaign they are creating.
- **Map editing:** Edit the map broadly, allowing the DM to move entire structures as well as terrain features, giving the DM an even higher level of control and creative freedom. This means that instead of relying on RNG for the map, the DM can use the RNG as a base, and make tweaks as they see fit.

# Constraints
Limits with the systems and technologies used when building and using the program.
- **Hardware:** Since I am using a MacBook air for the development and testing of the program, the program must be able to run on my computer.
- **Software:** Since I am using python and other python libraries to build the program, the program is constrained by the limits of Python. I likely will not run into many of Python's limitations but it is still a constraint none the less.
- **Time:** There is a time limit in the form of a deadline for this project, so I am constrained by the clock.

# Boundaries
Self-imposed limits when building and using the program.

- **Size:** In theory, the maps can be as large as I want, but I will need to limit this to a reasonable size so that the program is less CPU heavy, and so that the worlds aren't too big. If the world is too big, campaigns will be too long.

- **Themes:** I am limiting the amount of themes available as if I didn't, I would have to create a lot more assets for the program as each theme will have different visuals.

- **Scope:** I am limiting myself with the scope of this project so that I am able to build everything within the time constraint. This means there will be some features that I won't implement due to the complexity and the need to create a fully functioning program.

- **Not creating an executable:** I am not compiling the program into an executable so the user will need to know basic scripting and will have to download the libraries themselves.
