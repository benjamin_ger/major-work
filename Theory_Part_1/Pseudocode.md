# Pseudocode Algorithms

### Locate Towns

LocateTowns performs a linear search across the map png, which is treated as a 2D array. It breaks the 800x800 map into 25x25 pixel tiles, essentially splitting the bitmap into a 32x32 area of tile. The search then checks the colour values of the tiles, such that if at least 95% of a tile has a green value above 70, not including the colour (160,160,220), which is ice, then that tile is marked as suitable for a town to be placed on. The algorithm then skips the tiles adjacent to the tile that has been marked as suitable. This is to ensure that towns can't generate directly adjacent to each other. In summary, LocateTowns is a linear search algorithm that searches for and stores the location of suitable places to generate towns.

```C
BEGIN LocateTowns
    GET Image   /*Image is an RGB bitmap of the map*/
    xPass = 0
    yPass = 0
    FOR x = 0 TO 31 STEP 1
        IF xPass == 0 THEN
            FOR y = 0 TO 31 STEP 1
                IF yPass == 0 THEN
                    CorrectColours = []
                    FOR i = x*25 TO (x+1)*25 STEP 1
                        FOR j = x*25 TO (x+1)*25 STEP 1
                            IF Image[i][j][1] >= 70 AND Image[i][j] != [160,160,220] THEN
                                CorrectColours APPEND (i,j)
                            ENDIF
                        NEXT j
                    NEXT i
                    IF LENGTH CorrectColours >= 594 THEN  /*594 is 95% of 625 (25*25)*/
                        Locations APPEND (25*y,25*x)
                        xPass = 1
                        yPass = 1
                    ENDIF
                ELSE
                    yPass = 0
                ENDIF
            NEXT y
        ELSE
            xPass = 0
        ENDIF
    NEXT x
END LocateTowns
```

### Create Main Road

CreateMainRoad uses a pathfinding algorithm to generate roads that are organic and curved. There are 4 main roads that all start from the town centre, a random point in the vicinity of the screen's centre. At the town centre, each main road branches off at 90 degree increments so that there is no strange overlapping behaviour. Each road also is assigned a random point along the edge of the screen it is facing. Essentially CreateMainRoad path-finds to its destination (point on the edge of the screen) from the town centre. The points are then stored in an array, and that array is stored in another array, of which each line is stored in so that its points are accessible for reference when generating houses, but house generation is not performed in this algorithm.

```C
BEGIN CreateMainRoad
    GET Centre  /*Tuple in the form (x,y)*/
    GET points  /*Array of line end points*/
    FOR i = 0 TO 3 STEP 1
        Point1 = (0,0)
        angle = 0
        IF i == 0 THEN
            Point1 = (10*(random number between 0 and 80), 0)
            angle = -90
        ELSEIF i == 1 THEN
            Point1 = (800, 10*(random number between 0 and 80))
            angle = 0
        ELSEIF i == 2 THEN
            Point1 = (10*(random number between 0 and 80), 800)
            angle = 90
        ELSEIF i == 3 THEN
            Point1 = (0, 10*(random number between 0 and 80))
            angle = 180
        ENDIF
        CurrentPoint = [Centre[0], Centre[1]]
        points APPEND Point1
        line = []
        FOR i = 0 TO 150 STEP 1
            line APPEND (CurrentPoint[0], CurrentPoint[1])
            IF line[-1][0] > 0 AND line[-1][0] < 800 AND line[-1][1] > 0 AND line[-1][1] < 800 THEN
                turnRemaining = (atan2((Point1[1]-CurrentPoint[1])*pi/180, (Point1[0]-CurrentPoint[0])*pi/180))*180/pi - angle /*turnRemaining is how far away in degrees, the current orientation of the line is from facing its destination, which is Point1*/
                IF turnRemaining > 180 THEN
                    turnRemaining = turnRemaining - 360
                ELSEIF turnRemaining < -180 THEN
                    turnRemaining = turnRemaining + 360
                ENDIF
                IF turnRemaining < -1 THEN
                    angle = angle - 1
                ELSEIF turnRemaining > 1 THEN
                    angle = angle + 1
                ENDIF

                CurrentPoint[0] += int(10*cos(angle*pi/180))
                CurrentPoint[1] += int(10*sin(angle*pi/180))
            ELSE
                break
            ENDIF
        NEXT i
        lines APPEND line
    NEXT i
END CreateMainRoad
```
### Create Houses

create_house is an algorithm (I left it in python) that returns house polygons. A house polygon is formatted as a list of points that can be used to draw a polygon in the position and rotation of a valid house. For each road (stored in the list lines), a maximum amount of houses is determined as being a random number between 1/6th and 1/3rd the amount of points in the line. The reason there is a cut and cut2 variable as well as a Normal and Normal2 variable is because houses can generate on either side of the road. To test if a house is valid, a polygon is created based on the angle of the road at the point of generation. If that polygon intersects with any other previously generated houses, or if the polygon intersects with any line segment, then it is unsuitable and will not generate. The number of houses is random within a set range specified earlier and likewise the positions where houses are generated are random within the first 2/3rds of each road. This is so that houses in the towns are generated more towards the town centre, making it more realistic.

```Python
def create_house():
    for line in lines:
        num_houses = r.randint(int(len(line)/6), int(2*len(line)/3))
        for i in range(num_houses):
            pos = r.randint(4, int(2*len(line)/3))
            pos2 = r.randint(4, int(2*len(line)/3))
            if i + 1 < len(line):
                cut = False
                cut2 = False
                Normal = -90 + (atan2((line[pos][1]-line[pos+1][1])*pi/180, (line[pos][0]-line[pos+1][0])*pi/180))*180/pi
                Normal2 = 90 + (atan2((line[pos2][1]-line[pos2+1][1])*pi/180, (line[pos2][0]-line[pos2+1][0])*pi/180))*180/pi

                for h in houses:
                    if Polygon([ (line[pos][0] + 15*cos(Normal*pi/180), line[pos][1] + 15*sin(Normal*pi/180)),  (line[pos][0] + 30*cos((Normal+90)*pi/180) + 15*cos((Normal)*pi/180), line[pos][1] + 30*sin((Normal+90)*pi/180) + 15*sin((Normal)*pi/180)),  (line[pos][0] + 30*cos((Normal+90)*pi/180) + 50*cos((Normal)*pi/180), line[pos][1] + 50*sin((Normal)*pi/180)+ 30*sin((Normal+90)*pi/180)) ,  (line[pos][0] + 50*cos((Normal)*pi/180), line[pos][1] + 50*sin((Normal)*pi/180))  ]).intersects( box(h[0][0], h[0][1], h[2][0], h[2][1] )):
                        cut = True
                        break

                if cut == False:
                    for e in lines:
                        for a in range(len(e)):
                            if a + 1 < len(e):
                                if Polygon([ (line[pos][0] + 15*cos(Normal*pi/180), line[pos][1] + 15*sin(Normal*pi/180)),  (line[pos][0] + 30*cos((Normal+90)*pi/180) + 15*cos((Normal)*pi/180), line[pos][1] + 30*sin((Normal+90)*pi/180) + 15*sin((Normal)*pi/180)),  (line[pos][0] + 30*cos((Normal+90)*pi/180) + 50*cos((Normal)*pi/180), line[pos][1] + 50*sin((Normal)*pi/180)+ 30*sin((Normal+90)*pi/180)) ,  (line[pos][0] + 50*cos((Normal)*pi/180), line[pos][1] + 50*sin((Normal)*pi/180))  ]).intersects( LineString([ e[a], e[a + 1] ])):
                                    cut = True
                                    break

                if cut == False:
                    houses.append([ (line[pos][0] + 20*cos(Normal*pi/180), line[pos][1] + 20*sin(Normal*pi/180)),  (line[pos][0] + 20*cos((Normal+90)*pi/180) + 20*cos((Normal)*pi/180), line[pos][1] + 20*sin((Normal+90)*pi/180) + 20*sin((Normal)*pi/180)),  (line[pos][0] + 20*cos((Normal+90)*pi/180) + 40*cos((Normal)*pi/180), line[pos][1] + 40*sin((Normal)*pi/180)+ 20*sin((Normal+90)*pi/180)) ,  (line[pos][0] + 40*cos((Normal)*pi/180), line[pos][1] + 40*sin((Normal)*pi/180)), Normal  ])

                for h in houses:
                    if Polygon([ (line[pos2][0] + 15*cos(Normal2*pi/180), line[pos2][1] + 15*sin(Normal2*pi/180)),  (line[pos2][0] + 30*cos((Normal2+90)*pi/180) + 15*cos((Normal2)*pi/180), line[pos2][1] + 30*sin((Normal2+90)*pi/180) + 15*sin((Normal2)*pi/180)),  (line[pos2][0] + 30*cos((Normal2+90)*pi/180) + 50*cos((Normal2)*pi/180), line[pos2][1] + 50*sin((Normal2)*pi/180)+ 30*sin((Normal2+90)*pi/180)) ,  (line[pos2][0] + 50*cos((Normal2)*pi/180), line[pos2][1] + 50*sin((Normal2)*pi/180))  ]).intersects( box(h[0][0], h[0][1], h[2][0], h[2][1] )):
                        cut2 = True
                        break

                if cut2 == False:
                    for e in lines:
                        for a in range(len(e)):
                            if a + 1 < len(e):
                                if Polygon([ (line[pos2][0] + 15*cos(Normal2*pi/180), line[pos2][1] + 15*sin(Normal2*pi/180)),  (line[pos2][0] + 30*cos((Normal2+90)*pi/180) + 15*cos((Normal2)*pi/180), line[pos2][1] + 30*sin((Normal2+90)*pi/180) + 15*sin((Normal2)*pi/180)),  (line[pos2][0] + 30*cos((Normal2+90)*pi/180) + 50*cos((Normal2)*pi/180), line[pos2][1] + 50*sin((Normal2)*pi/180)+ 30*sin((Normal2+90)*pi/180)) ,  (line[pos2][0] + 50*cos((Normal2)*pi/180), line[pos2][1] + 50*sin((Normal2)*pi/180))  ]).intersects( LineString([ e[a], e[a + 1] ])):
                                    cut2 = True
                                    break

                if cut2 == False:
                    houses.append([ (line[pos2][0] + 20*cos(Normal2*pi/180), line[pos2][1] + 20*sin(Normal2*pi/180)),  (line[pos2][0] + 20*cos((Normal2+90)*pi/180) + 20*cos((Normal2)*pi/180), line[pos2][1] + 20*sin((Normal2+90)*pi/180) + 20*sin((Normal2)*pi/180)),  (line[pos2][0] + 20*cos((Normal2+90)*pi/180) + 40*cos((Normal2)*pi/180), line[pos2][1] + 40*sin((Normal2)*pi/180)+ 20*sin((Normal2+90)*pi/180)) ,  (line[pos2][0] + 40*cos((Normal2)*pi/180), line[pos2][1] + 40*sin((Normal2)*pi/180)), Normal2 ])
```

### Main Control Structure

The main control structure of my program will determine how processes are executed and how the user will interact with the system. This section hasn't been prototyped in python yet so it will be more complex when built in python to account for niche factors as well as edge-case scenarios that may arise in development.

```C
BEGIN SystemControl
    GET user inputs
    IF the user clicks 'LOAD' THEN
        WHILE TRUE
            GET User file choice
            IF the user clicks 'GO' THEN
                LOAD User file choice
                BREAK
            ENDIF
        ENDWHILE
    ELSEIF the user clicks 'NEW' THEN
        WHILE TRUE
            GET User world settings
            IF the user clicks 'GO' THEN
                GENERATE from User world settings
                BREAK
            ENDIF
        ENDWHILE
    ENDIF


    WHILE TRUE
        GET user inputs
        IF the user clicked on a town THEN
            IF the user clicked on a building THEN
                IF the user clicked on an interactable THEN
                    DISPLAY interactable information
                ELSE
                    DISPLAY building map
                ENDIF
            ELSE
                DISPLAY town map
            ENDIF
        ELSE
            DISPLAY world map
        ENDIF

        IF the user clicked 'SAVE' THEN
            SAVE user world
        ENDIF

        IF the user clicked 'QUIT' THEN
            BREAK
        ENDIF
    ENDWHILE
END System
```
