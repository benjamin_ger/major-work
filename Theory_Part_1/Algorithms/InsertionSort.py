import random as r

list = []
Finish = False
cache = 0
Current = 1
Compare = 0
movingItem = Current

def create_random_list():
    for i in range(r.randint(5,20)):
        list.append(r.randint(0,100))

create_random_list()
print(list)

while Current < len(list):
    cache = list[Current]
    Compare = 0
    Finish = False
    while Compare < Current and Finish == False:
        if cache < list[Compare]:
            movingItem = Current
            while movingItem > Compare:
                list[movingItem] = list[movingItem - 1]
                movingItem -= 1

            list[Compare] = cache
            Finish = True

        Compare += 1

    Current += 1

print(list)
