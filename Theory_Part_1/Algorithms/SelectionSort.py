import random as r

list = []
cache = (0, 0)

def create_random_list():
    for i in range(r.randint(5,20)):
        list.append(r.randint(0,100))

create_random_list()
print(list)

for i in range(len(list)):
    cache = (list[i], i)
    for j in range(i, len(list)):
        if list[j] < cache[0]:
            cache = (list[j], j)

    if list[cache[1]] != list[i]:
        list[cache[1]] = list[i]
        list[i] = cache[0]

print(list)
