import random as r

list = []
Search = r.randint(0,100)
Found = False

def create_random_list():
    for i in range(r.randint(5,20)):
        list.append(r.randint(0,100))

create_random_list()
print("\nSearch for", Search, "in:")
print(list)

for i in range(len(list)):
    if Search == list[i]:
        Found = True
        print(Search, "found at position:", i + 1, "\n")
        break

if Found == False:
    print(Search, "not in list\n")
