import random as r

list = []
Swapped = True
cache = 0
count = 0

def create_random_list():
    for i in range(r.randint(5,20)):
        list.append(r.randint(0,100))

create_random_list()
print(list)

while Swapped:
    Swapped = False
    count += 1
    for i in range(len(list)):
        if i + 1 < len(list):
            if list[i] > list[i + 1]:
                Swapped = True
                cache = list[i]
                list[i] = list[i + 1]
                list[i + 1] = cache

print(list)
print(count)
