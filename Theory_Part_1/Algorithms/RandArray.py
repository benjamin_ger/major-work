import random

def RandArray(min, max, num):
    rNum = []
    arr = []
    for i in range(0, max-min):
        rNum.append(i+min)

    for i in range(0, num - 1):
        r = random.randint(i, max-min-1)
        arr.append(rNum[r])
        rNum[r] = rNum[i]

    return arr

print(RandArray(0, 10, 10))
