import random as r

Word = ''

def Generate_String():
    global Word
    letters = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z']
    for i in range(r.randint(15, 30)):
        l = r.randint(0,len(letters)-1)
        Word += letters[l]

    print('\n'+Word)


def Delete():
    Temp = ''
    Start = r.randint(0, len(Word)-2)
    End = r.randint(Start, len(Word)-1)
    print(Start+1, End+1)
    for i in range(0,Start):
        Temp += Word[i]

    for i in range(End+1, len(Word)):
        Temp += Word[i]

    return Temp


Generate_String()
print(Delete()+'\n')
