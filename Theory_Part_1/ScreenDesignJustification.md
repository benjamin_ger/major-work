# Screen Design Justification

### Features

##### Screen Design 1
Screen Design 1 is the startup config screen for my program. In this section of the program, The user can decide whether to generate a new world or load in a pre-existing world. For generating a new world, features that the user has access to is the theme of the world, as well as how much of the world is snow. For loading in a pre-existing world, the user is able to select a world file from a list of files that the program identified as valid.

##### Screen Design 2
Screen Design 2 is the world map, displaying a view of the entire generated world. Features available include an interactive map, where you can click on towns and ruins and travel to them, while also displaying more info about the town or ruin. There is also a "Save" button, that saves the world to a local file if you decide that you like the world, and a "Quit" button to close the program. There is also a description section on the left hand side of the screen that displays information about the selected town or ruin.

##### Screen Design 3
Screen Design 3 is the town map, displaying a view of the town that the you have travelled to from the world map. It has the same feature set as the world map, except you can click on and travel to buildings instead of towns and ruins. It also has the same "Save" and "Quit" buttons.

##### Screen Design 4
Screen Design 4 is the building map, displaying a floor-plan view of the building that you have travelled to from the town map. You are able to click on objects and residents that populate the building and it will display relevant information about them on the left hand side of the screen. Likewise it also has the same "Save" and "Quit" buttons.

##### Screen Design 5
Screen Design 5 is the ruin map, displaying a view of the ruin that you have travelled to from the world map. You are able to click on objects that populate the ruin and it will display relevant information about them on the left hand side of the screen. It too also has the same "Save" and "Quit" buttons.

### GUI
Ergonomics are a very important aspect of the GUI, as it relates to how easy, intuitive and accessible the software is to use. It should be said that my screen designs are in black and white, but the actual product will be in colour, this will make some features much more obvious than they might otherwise appear in the screen design. An example of this is the circling. When a town, ruin, building or object is selected, a circle appears around it. This will be bright red for high contrast, as most of the images will be blue, green and white, making red a high contrast colour option. One thing I have went for with the screen designs is a simple set of controls, with few buttons that are in consistent and intuitive locations. For example, the "Save" and "Quit" buttons are in the same place for all the screen designs, meaning that the user will always know where they are. This same principle applies to all aspects of my screen designs. I specifically made it such that each screen is formatted the same, so that it is consistent and easy to use. This means that the Name of the area you are in is above the main image for all screens, the name and description of the selected item is in the same area for all screens, and the buttons are in the same area for all screens. Another important aspect for GUI ergonomics is contrast. I touched on this with the red circle around selected items, but this will also extend to all aspects of the screen. There will be high contrast between text and background, high contrast between buttons and background, and high contrast in the boarders around screen elements. This is important so that so that buttons and features are easy to distinguish. In summary, I have designed by GUI to be consistent and high contrast for the sake of ease of use, with controls that are intuitive and simple to use.
