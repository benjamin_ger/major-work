# Problem Definition

### Problems
DnD is a very flexible tabletop RPG, with a huge variety of official and custom campaigns. One of the problems with building your own DnD campaign is world building. Most people who build campaigns create a world centred around the narrative of the campaign. The problem with this is that the world isn't that developed, with only major structures and key storyline areas being written. In short, many people who build campaigns struggle to build a fleshed out world to go with it, and those who do often spend a lot of time doing so. Another problem that people face when building a campaign is writers block. Sometimes it is difficult to know where to take the campaign next as it is not always obvious.

### Solutions
I want to build a program that can randomly generate DnD worlds using a few starting parameters. These worlds will easily lend themselves to campaigns, and will have areas and monsters that can easily be used as inspiration or as a structure for a campaign. This program will reverse the order of campaign building. Instead of building a campaign, then building the world, this program will build a world that can inspire a campaign. This makes the building of campaigns more fluid, as if the DM is ever stuck, they can always lead the campaign to other areas of the map. This program will both solve the problem of underdeveloped worlds, and assist in quelling writers block, by providing inspiration to the DM.

### Implementation
The program I want to build will randomly generate DnD worlds and then display them as an interactable map. This will include a top-down view of the map. The user will be able to click in individual buildings to display their floor-plan to the user, as well as loot, enemies, and other interactable items. Descriptions of the rooms, loot, and enemies will be included to aid the DM when running the DnD session.
