# Reports

## Programming Languages Report

Choosing an ideal programming language is an important part of the project as different languages have different features and capabilities. My project will require lots of computation on startup but will then just display the GUI and handle events. The project will also be very object oriented. So in this case, I would want to use a programming language that can handle the computational load of world generation, as well as having a robust class system for creating and managing objects. The GUI component likely won't contribute too much to the choice of programming language as most languages can handle basic graphics and images.

#### C*#*
C*#* is a multi-paradigm programming language that is a C style language. Multi-paradigm meaning that it uses multiple programming paradigms; declarative, functional, and object oriented to name a few. This means that C*#* can be used to build classes and objects, as well as using functions and lambda expressions when programming, which can increase the flexibility and reusability of the code. C*#* is also a garbage collecting language, meaning it can handle memory management and reclamation. When a variable is assigned a value, or more commonly when an object is created, it will occupy a space in system memory, and without proper management, more and more objects will keep filling up memory depending on how the software works in relation to objects. Not all the objects created will be in use all the time, and objects that the software wants to delete would have to be disposed so that their memory space can be vacated. Garbage collection makes this process more streamlined, as well as makes the software safer from a resources perspective, as it can release memory from deleted objects and variables, as well as free up memory that rarely used objects occupy. In terms of this project, C*#* is a good option of programming language as it contains all the features I would need, most specifically, it is object oriented, which is important since my project will require the use of objects due to the more layered approach of having city objects that contain structure objects that contain interactable objects. C*#* is also well regarded for its speed as well, and should make an excellent choice knowing that world generation will be quite taxing. However, one of the problems that is stopping me from using C*#* is my lack of proficiency in it. Since my major work project encompasses a lot of new concepts, techniques and programming challenges, I feel that a project like this is not suitable for learning a language as well as it will require a lot of prerequisite experience. Since I have done some C*#* in the past, it is still viable, but I don't feel confident in my abilities with this language to tackle my project.

#### JavaScript
JavaScript is also a multi-paradigm programming language that is a C style language. In this case, event-driven, functional and object oriented programming paradigms. JavaScript is weakly typed, meaning that different data-types can be combined and evaluated without an error being thrown. JavaScript also has automated memory allocation and garbage collection. JavaScript is most commonly used along side HTML and CSS and is found in much of internet and websites. JavaScript engines are commonly found in browsers, meaning that most applications can easily be made to run inside a browser tab with little resistance. Because of this, JavaScript is a safe language; as in it doesn't provide access to low level CPU and memory resources. In terms of this project, JavaScript is viable as it has the tools and the speed to do it. But C*#* would be a better choice over JavaScript due to JavaScript being tailored more towards web applications, when this project is not a web application, or at least it doesn't have to be. This combined with the fact that I have limited experience with the language makes it a possibility, but not very likely to be used for this project.

#### Java
Java is an object oriented, C style language that is designed as a general purpose programming language for application developers. Java is strongly typed, meaning it is particular about how datatypes are used and evaluated. One of the features of Java is its ability to be compiled once and ran anywhere without recompilation, which is useful for this project which is designed to be opened and closed and reused. Like C*#* and JavaScript, Java too has automated memory allocation and garbage collection. Java has many short comings though. Java is very verbose and painful to work with, with simple tasks being overcomplicated regularly. Java is also known for being slow and for having atrocious integration with its UI, which just so happens to be the focal point and end product of my project. Java also uses JVM for compilation, meaning it gets compiled to a .jar file that is interpreted by the JVM, which is quite inefficient for my purposes. Because of these and my lack of proficiency in the language, Java is an unsuitable language for this project.

#### Python
Python is a multi-paradigm, general purpose programming language that is strongly typed. Python is object oriented, functional and procedural. Python is an interpreted language, meaning that instead of being compiled traditionally, it is executed line by line. This does make it slower than traditionally compiled languages like C*#* and JavaScript which uses just in time (JIT) compilation, but because it is interpreted, it is easier to debug as the program will run until it hits an error, outputting the line into console. Python has memory allocation and garbage collection too, and also has a large and comprehensive standard library that can be used to simplify the creation of some modules as well as the GUI. Python is useful in a variety of areas, from simple scripts to web applications and can easily be used for my project using libraries like pygame. I also happen to have a good amount of experience using python, so it won't be too difficult to integrate the new concepts and methods I will be using in this project. Another reason why I am choosing python over C*#* specifically is because C*#* and especially GUI tools for it are mostly built for Microsoft's .NET framework, which would mean that C*#* development, especially for me is much more difficult to do in MacOS and I would need to use a Windows PC. Python, however, works well and is easy to develop with in either operating system, making it less of a headache for me to use as I wouldn't need to use tools like Parallels.

## Emerging Technologies Report

#### AI
Artificial Intelligence is the capability for a computer to think, act and behave like a human so that it learn from its environment and make predictions about it. As AI is being explored, it is making its way into a large array of industries like cars, health, manufacturing, smart devices and large portion's of the internet's search, security and advertising sectors. This is relevant to my project as AI can be used for things like advanced pathfinding and image processing, both of which may need to be used when I integrate the different core modules of my program (map, towns, buildings) together so that they are coherent and fit together. For example, if I want to place a town on the map, AI could be useful for finding a suitable location for it based on the features of the town. AI could also be used to connect towns, as the pathfinding algorithm I will use to build roads inside the town isn't smart enough to identify and avoid objects like forests that will be present on the map. AI is a promising emerging technology and may impact the capabilities or development of my project.

#### Mobile Technology
Mobile Technology refers to mobile phones and their capacity as computers. Apple's release of the iPhone in January 2007 changed how we perceived mobile technology, with phones becoming more and more powerful, and industry pushing more and more resources into mobile apps and development. The main factor that has separated mobile phones and traditional computers and laptops is CPU architecture. x86 has existed in computers for decades and has become so ubiquitous that it is very difficult to change. Though x86 is power hungry and inefficient, and would likely fail in the form-factor of a phone. Because of this, phones use the ARM architecture, which is much more efficient and scaleable, meaning it can be used for high end and low power systems.

This difference in architecture between mobile and desktop computers have made it very difficult for developers to create software that works on both. There have been numerous attempts to bring ARM to the desktop, but most have failed to gain any traction due to insufficient hardware and low adoption in software. Windows for ARM is quite limited and ChromeOS is pretty much locked down. Apple's release of the M1 chip was the first big step towards ARM on the desktop, with performance well within the range of most x86 chips. Apple's Rosetta x86 emulator has also made using the ARM based computer seamless with x86 applications. Apple has been slowly expanding iPadOS towards MacOS, blurring the lines between mobile and desktop devices.

For my project, mobile technology is relevant to the use of my project as DMs would want to use it at games nights or the like, and having it available on mobile would make it easier to access and use.

## Current Issue in IT Report

#### Data Hoarding and Targeted Ads
As the internet has developed over the decades, so has technology in areas like AI and machine learning. Developments in these two areas has given rise to the targeted ad. Targeted advertising refers to serving advertisements to people based on information about them, such that the ad appeals to them as much as possible. AI and machine learning are important to this subject as they are used to retrieve data from users based on their internet activity, as well as to process that data and make a profile of the user, categorising them based on their interests. That data can then be sold to advertising companies so that you receive ads relating to your interests. This means that obtaining as much data about users as possible, data hoarding per se, is incentivised to large companies like Google, Facebook and Amazon, as data is effectively currency. This raises a number of social and ethical issues, such as privacy, security, and safety.

Data hoarding and targeted advertising raises major concerns for user privacy, mainly because companies like Google, Facebook and Amazon collect data and information about its users without their consent or knowledge. A lot of this data could be really personal, such as health and medical information. An example of where this becomes quite uncanny is when these companies can find out if you are pregnant before you even know, base off of changes in the food you order or what you look up. Sometimes these companies know more about you than you do, and they almost always have information about you that you otherwise wouldn't disclose to anyone. We are now subjects to the corporate race to collect the most amount of data possible. You don't even need a facebook account for them to create a profile of you.

These privacy concerns also raise the issues of security and safety. How are these companies going to use your data? Who will have access to it? What people do with your data? These are all questions we shouldn't have to ask but yet are big topics today. The issue of security relates to how your data is used and managed by the big companies. Facebook, for example has had many major data leaks, with 533 million accounts being made publicly available as of recent. These included names, phone numbers, emails, and locations, which is more than enough data for scammers and hackers to attack you or acquire more data. This now raises the issue of safety. If companies are storing sensitive data about its users, then user safety is at risk in the event of a leak or breach. Locations and home addresses poses the risk of the physical safety of the individual, whereas credit card information and bank details puts their money at risk. Plus, all the other sensitive or otherwise personal information that companies collect can put people's livelihood and social lives at risk. There is a whole lot of risk involved when collecting and storing data, and when it is done by an insecure or untrustworthy company then the people end up paying for it.


## Links & Access Dates

#### Programming Languages
- https://docs.microsoft.com/en-us/dotnet/csharp/tour-of-csharp/
02/03/21

- https://docs.microsoft.com/en-us/dotnet/standard/garbage-collection/
02/03/21

- https://www.geeksforgeeks.org/introduction-of-programming-paradigms/
04/03/21

- https://en.wikipedia.org/wiki/JavaScript
12/03/21

- https://javascript.info/intro
12/03/21

- https://developer.mozilla.org/en-US/docs/Web/JavaScript/Memory_Management
12/03/21

- https://en.wikipedia.org/wiki/Java_(programming_language)
15/03/21

- https://www.sciencedirect.com/topics/computer-science/java-programming-language
15/03/21

- https://en.wikipedia.org/wiki/Python_(programming_language)
15/03/21

- https://www.python.org/doc/essays/blurb/
16/03/21

#### Emerging Technologies
- https://www.europarl.europa.eu/news/en/headlines/society/20200827STO85804/what-is-artificial-intelligence-and-how-is-it-used#:~:text=Artificial%20intelligence%20is%20widely%20used,%2C%20planning%20inventory%2C%20logistics%20etc.
21/04/21

- https://en.wikipedia.org/wiki/Applications_of_artificial_intelligence
21/04/21

- https://en.wikipedia.org/wiki/Smartphone
27/04/21

- https://www.stuff.tv/in/features/opinion-apples-m1-chip-best-thing-happen-in-tech-cusp-smartphones
27/04/21

- https://ipsnews.net/business/2020/06/29/arm-vs-x86-instruction-sets-architecture-and-all-key-differences-explained/
27/04/21

#### Current Issue in IT
- https://blog.malwarebytes.com/malwarebytes-news/2021/05/facebook-bans-signal-ads-that-reveal-the-depth-of-what-it-knows-about-you/
10/5/21

- https://edu.gcfglobal.org/en/thenow/what-is-targeted-advertising/1/
10/5/21

- https://www.npr.org/2021/04/09/986005820/after-data-breach-exposes-530-million-facebook-says-it-will-not-notify-users
10/5/21
