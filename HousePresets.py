import random as r      #This file stores all the presets for the interiors of houses and is called when house interiors are generated
import pygame

class Interactable:     #Interactable class stores data about itself such as its image and a loot table
    def __init__(self, name, ID, description, LootTableName):
        self.name = name
        self.description = description
        self.ID = ID
        self.LootTableName = LootTableName
        self.image = pygame.image.load("Images/Interactables/"+str(ID)+".png").convert_alpha()
        self.inventory = []
        self.LootTable()

    def LootTable(self):    #Generates loot in the inventory from a loot table
        if self.LootTableName != 0:
            table = open("Text/LootTables/" + self.LootTableName + ".txt", "r")
            for line in table:
                loot = line[:-1].split("|")
                if r.randint(0,100) >= int(loot[2]):
                    amount = r.randint(int(loot[0]), int(loot[1]))
                    if amount > 0:
                        self.inventory.append([amount, loot[3]])

def HousePreset(interactables):     #Preset used for the house and hut
    interactables[r.randint(1,8)][9] = Interactable( "Door", 1, 'A simple wooden|door.', 0)

    bed = r.randint(0,7)
    interactables[bed][0] = Interactable( "Bed", 5, "A simple bed.", 0 )
    interactables[bed][1] = Interactable( "Bed", 6, "A simple bed.", 0 )
    interactables[bed+2][0] = Interactable( "Bed", 5, "A simple bed.", 0 )
    interactables[bed+2][1] = Interactable( "Bed", 6, "A simple bed.", 0 )

    tableX = r.randint(2,6)
    tableY = r.randint(3,5)
    interactables[tableX][tableY] = Interactable( "Table", 7, "A large wooden|table.", 0 )
    interactables[tableX+1][tableY] = Interactable( "Table", 8, "A large wooden|table.", 0 )
    interactables[tableX][tableY+1] = Interactable( "Table", 9, "A large wooden|table.", 0 )
    interactables[tableX+1][tableY+1] = Interactable( "Table", 10, "A large wooden|table.", 0 )
    interactables[tableX][tableY+2] = Interactable( "Table", 11, "A large wooden|table.", 0 )
    interactables[tableX+1][tableY+2] = Interactable( "Table", 12, "A large wooden|table.", 0 )

    amount1 = r.randint(1,3)
    amount2 = r.randint(1,3)
    Storage1 = r.randint(4,10-amount1)
    Storage2 = r.randint(4,10-amount2)
    for i in range(amount1):
        interactables[0][Storage1-1+i] = Interactable( "Barrel", 4, "A storage barrel.", "HouseBarrel" )
    for i in range(amount2):
        interactables[9][Storage2-1+i] = Interactable( "Barrel", 4, "A storage barrel.", "HouseBarrel" )

def TowerPreset(interactables):     #Preset used for the tower
    interactables[r.randint(1,8)][9] = Interactable( "Door", 1, 'A simple wooden|door.', 0)

    amount1 = r.randint(2,4)
    amount2 = r.randint(2,4)
    amount3 = r.randint(2,4)
    Storage1 = r.randint(1,9-amount1)
    Storage2 = r.randint(1,9-amount2)
    Storage3 = r.randint(0,9-amount3)
    for i in range(amount1):
        interactables[0][Storage1+i] = Interactable( "Barrel", 4, "A storage barrel.", "TowerBarrel" )
    for i in range(amount2):
        interactables[9][Storage2+i] = Interactable( "Barrel", 4, "A storage barrel.", "TowerBarrel" )
    for i in range(amount3):
        interactables[Storage2+i][0] = Interactable( "Barrel", 4, "A storage barrel.", "TowerBarrel" )

    tableX = r.randint(2,6)
    tableY = r.randint(3,5)
    interactables[tableX][tableY] = Interactable( "Table", 7, "A large wooden|table.", 0 )
    interactables[tableX+1][tableY] = Interactable( "Table", 8, "A large wooden|table.", 0 )
    interactables[tableX][tableY+1] = Interactable( "Table", 9, "A large wooden|table.", 0 )
    interactables[tableX+1][tableY+1] = Interactable( "Table", 10, "A large wooden|table.", 0 )
    interactables[tableX][tableY+2] = Interactable( "Table", 11, "A large wooden|table.", 0 )
    interactables[tableX+1][tableY+2] = Interactable( "Table", 12, "A large wooden|table.", 0 )


def ChurchPreset(interactables):    #Preset used for the church
    interactables[4][19] = Interactable( "Door", 1, 'A simple wooden|door.', 0)
    interactables[5][19] = Interactable( "Door", 3, 'A simple wooden|door.', 0 )

    for i in range(7):
        interactables[0][4+2*i] = Interactable( "Pew", 13, "A church pew.", 0 )
        interactables[1][4+2*i] = Interactable( "Pew", 14, "A church pew.", 0 )
        interactables[2][4+2*i] = Interactable( "Pew", 14, "A church pew.", 0 )
        interactables[3][4+2*i] = Interactable( "Pew", 15, "A church pew.", 0 )

        interactables[6][4+2*i] = Interactable( "Pew", 13, "A church pew.", 0 )
        interactables[7][4+2*i] = Interactable( "Pew", 14, "A church pew.", 0 )
        interactables[8][4+2*i] = Interactable( "Pew", 14, "A church pew.", 0 )
        interactables[9][4+2*i] = Interactable( "Pew", 15, "A church pew.", 0 )

    interactables[2][1] = Interactable( "Lectern", 16, "A church lectern.", "Lectern" )

    interactables[4][1] = Interactable( "Altar", 17, "The altar.", "Altar" )
    interactables[5][1] = Interactable( "Altar", 18, "The altar.", "Altar" )

    interactables[7][0] = Interactable( "Organ", 19, "Church organ.", 0 )
    interactables[8][0] = Interactable( "Organ", 20, "Church organ.", 0 )

def BlacksmithPreset(interactables):    #Preset used for the blacksmith
    interactables[r.randint(1,8)][19] = Interactable( "Door", 1, 'A simple wooden|door.', 0)
    interactables[9][r.randint(3,8)] = Interactable( "Door", 2, 'A simple wooden|door.', 0 )

    interactables[8][0] = Interactable( "Fireplace", 21, "A large fireplace.", "Fireplace" )
    interactables[9][0] = Interactable( "Fireplace", 22, "A large fireplace.", "Fireplace" )
    interactables[8][1] = Interactable( "Fireplace", 23, "A large fireplace.", "Fireplace" )
    interactables[9][1] = Interactable( "Fireplace", 24, "A large fireplace.", "Fireplace" )

    bed = r.randint(0,2)
    interactables[bed][0] = Interactable( "Bed", 5, "A simple bed.", 0)
    interactables[bed][1] = Interactable( "Bed", 6, "A simple bed.", 0)
    interactables[bed+2][0] = Interactable( "Bed", 5, "A simple bed.", 0)
    interactables[bed+2][1] = Interactable( "Bed", 6, "A simple bed.", 0)
    interactables[bed+5][0] = Interactable( "Bed", 5, "A simple bed.", 0)
    interactables[bed+5][1] = Interactable( "Bed", 6, "A simple bed.", 0)

    tableX = r.randint(2,6)
    tableY = r.randint(11,14)
    interactables[tableX][tableY] = Interactable( "Table", 7, "A large wooden|table.", 0 )
    interactables[tableX+1][tableY] = Interactable( "Table", 8, "A large wooden|table.", 0 )
    interactables[tableX][tableY+1] = Interactable( "Table", 9, "A large wooden|table.", 0 )
    interactables[tableX+1][tableY+1] = Interactable( "Table", 10, "A large wooden|table.", 0 )
    interactables[tableX][tableY+2] = Interactable( "Table", 11, "A large wooden|table.", 0 )
    interactables[tableX+1][tableY+2] = Interactable( "Table", 12, "A large wooden|table.", 0 )

    amount1 = r.randint(1,3)
    amount2 = r.randint(1,4)
    Storage1 = r.randint(10,20-amount1)
    Storage2 = r.randint(10,20-amount2)
    for i in range(amount1):
        interactables[0][Storage1-1+i] = Interactable( "Barrel", 4, "A storage barrel.", "HouseBarrel" )
    for i in range(amount2):
        interactables[9][Storage2-1+i] = Interactable( "Barrel", 4, "A storage barrel.", "HouseBarrel" )


    interactables[18][0] = Interactable( "Forge", 21, "The blacksmith's|forge.", "Forge" )
    interactables[19][0] = Interactable( "Forge", 22, "The blacksmith's|forge.", "Forge" )
    interactables[18][1] = Interactable( "Forge", 23, "The blacksmith's|forge.", "Forge" )
    interactables[19][1] = Interactable( "Forge", 24, "The blacksmith's|forge.", "Forge" )

    toolstorage = r.randint(10,15)
    amount3 = r.randint(2,4)
    for i in range(amount3):
        interactables[toolstorage+i][9] = Interactable( "Tool Rack", 25, "The blacksmith's|toolrack.", "ToolRack" )

    amount4 = r.randint(1,3)
    for i in range(amount4):
        anvilX = r.randint(12,13)
        anvilY = r.randint(2,7)
        interactables[anvilX+i][anvilY] = Interactable( "Anvil", 26, "The blacksmith's|anvil.", 0 )

    cauldron = r.randint(11,14)
    amount5 = r.randint(1,3)
    for i in range(amount5):
        interactables[cauldron+i][0] = Interactable( "Cauldron", 27, "The blacksmith's|cauldron.", 0 )

def TavernPreset(interactables):      #Preset used for the tavern
    interactables[r.randint(1,8)][19] = Interactable( "Door", 1, 'A simple wooden|door.', 0)

    tableX1 = r.randint(0,2)
    tableY1 = r.randint(0,3)
    interactables[tableX1][tableY1] = Interactable( "Table", 7, "A large wooden|table.", 0 )
    interactables[tableX1+1][tableY1] = Interactable( "Table", 8, "A large wooden|table.", 0 )
    interactables[tableX1][tableY1+1] = Interactable( "Table", 9, "A large wooden|table.", 0 )
    interactables[tableX1+1][tableY1+1] = Interactable( "Table", 10, "A large wooden|table.", 0 )
    interactables[tableX1][tableY1+2] = Interactable( "Table", 11, "A large wooden|table.", 0 )
    interactables[tableX1+1][tableY1+2] = Interactable( "Table", 12, "A large wooden|table.", 0 )

    tableX2 = r.randint(5,7)
    tableY2 = r.randint(0,3)
    interactables[tableX2][tableY2] = Interactable( "Table", 7, "A large wooden|table.", 0 )
    interactables[tableX2+1][tableY2] = Interactable( "Table", 8, "A large wooden|table.", 0 )
    interactables[tableX2][tableY2+1] = Interactable( "Table", 9, "A large wooden|table.", 0 )
    interactables[tableX2+1][tableY2+1] = Interactable( "Table", 10, "A large wooden|table.", 0 )
    interactables[tableX2][tableY2+2] = Interactable( "Table", 11, "A large wooden|table.", 0 )
    interactables[tableX2+1][tableY2+2] = Interactable( "Table", 12, "A large wooden|table.", 0 )

    tableX3 = r.randint(0,2)
    tableY3 = r.randint(7,9)
    interactables[tableX3][tableY3] = Interactable( "Table", 7, "A large wooden|table.", 0 )
    interactables[tableX3+1][tableY3] = Interactable( "Table", 8, "A large wooden|table.", 0 )
    interactables[tableX3][tableY3+1] = Interactable( "Table", 9, "A large wooden|table.", 0 )
    interactables[tableX3+1][tableY3+1] = Interactable( "Table", 10, "A large wooden|table.", 0 )
    interactables[tableX3][tableY3+2] = Interactable( "Table", 11, "A large wooden|table.", 0 )
    interactables[tableX3+1][tableY3+2] = Interactable( "Table", 12, "A large wooden|table.", 0 )

    tableX4 = r.randint(5,7)
    tableY4 = r.randint(7,9)
    interactables[tableX4][tableY4] = Interactable( "Table", 7, "A large wooden|table.", 0 )
    interactables[tableX4+1][tableY4] = Interactable( "Table", 8, "A large wooden|table.", 0 )
    interactables[tableX4][tableY4+1] = Interactable( "Table", 9, "A large wooden|table.", 0 )
    interactables[tableX4+1][tableY4+1] = Interactable( "Table", 10, "A large wooden|table.", 0 )
    interactables[tableX4][tableY4+2] = Interactable( "Table", 11, "A large wooden|table.", 0 )
    interactables[tableX4+1][tableY4+2] = Interactable( "Table", 12, "A large wooden|table.", 0 )

    tableX5 = r.randint(0,2)
    tableY5 = r.randint(14,16)
    interactables[tableX5][tableY5] = Interactable( "Table", 7, "A large wooden|table.", 0 )
    interactables[tableX5+1][tableY5] = Interactable( "Table", 8, "A large wooden|table.", 0 )
    interactables[tableX5][tableY5+1] = Interactable( "Table", 9, "A large wooden|table.", 0 )
    interactables[tableX5+1][tableY5+1] = Interactable( "Table", 10, "A large wooden|table.", 0 )
    interactables[tableX5][tableY5+2] = Interactable( "Table", 11, "A large wooden|table.", 0 )
    interactables[tableX5+1][tableY5+2] = Interactable( "Table", 12, "A large wooden|table.", 0 )

    tableX6 = r.randint(5,7)
    tableY6 = r.randint(14,16)
    interactables[tableX6][tableY6] = Interactable( "Table", 7, "A large wooden|table.", 0 )
    interactables[tableX6+1][tableY6] = Interactable( "Table", 8, "A large wooden|table.", 0 )
    interactables[tableX6][tableY6+1] = Interactable( "Table", 9, "A large wooden|table.", 0 )
    interactables[tableX6+1][tableY6+1] = Interactable( "Table", 10, "A large wooden|table.", 0 )
    interactables[tableX6][tableY6+2] = Interactable( "Table", 11, "A large wooden|table.", 0 )
    interactables[tableX6+1][tableY6+2] = Interactable( "Table", 12, "A large wooden|table.", 0 )

    for i in range(8):
        interactables[12][i] = Interactable( "Bar", 28, "The bar.", "Bar" )
        if r.randint(1,2) == 1:
            interactables[11][i] = Interactable("Barstool", 30, "A round chair.", 0)

    interactables[12][8] = Interactable( "Bar", 29, "The bar.", "Bar" )

    for i in range(15, 19):
        if r.randint(1,2) == 1:
            interactables[i][0] = Interactable( "Barrel", 4, "A storage barrel.", "TavernBarrel" )
        if r.randint(1,2) == 1:
            interactables[i][9] = Interactable( "Barrel", 4, "A storage barrel.", "TavernBarrel" )

    for i in range(1, 8):
        if r.randint(1,2) == 1:
            interactables[19][i] = Interactable( "Barrel", 4, "A storage barrel.", "TavernBarrel" )
